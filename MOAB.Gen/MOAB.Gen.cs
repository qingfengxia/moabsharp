﻿
using System;
using System.IO;
using System.Runtime.InteropServices;

using CppSharp;
using CppSharp.AST;
using CppSharp.Passes;

namespace CppSharp
{
    class MOAB : CppSharp.ILibrary
    {
        public void Setup(Driver driver)
        {
            var options = driver.Options;
            options.Verbose = true;
            var module = options.AddModule("MOAB");

            // MOAB_DIR
            var moab_dir = Environment.GetEnvironmentVariable("MOAB_DIR");  // not working, why?
            //var moab_dir = "/home/qxia/Repositories/dagmc_bld/MOAB";
            Console.WriteLine($" MOAB_DIR = {moab_dir}");
            var moab_header_dir = Path.Combine(moab_dir, "include");
            if (! Directory.Exists(moab_header_dir))
            {
                throw new IOException(string.Format("{0} does not exist!", moab_header_dir));
            }

            module.IncludeDirs.Add(moab_header_dir);
            //parserOptions.AddIncludeDirs(moab_header_dir);  // outdated API


            module.Headers.Add("moab/iMOAB.h");
            module.Headers.Add("iMesh.h");

            module.Headers.Add("moab/FileOptions.hpp");   // always fwd declare, CppSharp does not know where is the def
            module.Headers.Add("MBTagConventions.hpp");   // macro constants are not exported?
            module.Headers.Add("moab/Types.hpp"); 
            module.Headers.Add("moab/Interface.hpp");
            module.Headers.Add("moab/Core.hpp");   // this header include "moab/Interface.hpp" 
            module.Headers.Add("moab/GeomTopoTool.hpp");      
 

            options.GenerateClassTemplates = false;  // does not skip std::vector<>
            options.GenerateDefaultValuesForArguments = true;
            options.UsePropertyDetectionHeuristics = false;  // avoid some function is treated as Property
            //var parserOptions = driver.ParserOptions;
            //DriverOptions.GenerateFunctionTemplates

            // default values for windows
            var moab_so = "MOAB.dll";
            var imesh_so = "iMesh.dll";
            module.LibraryDirs.Add(Path.Combine(moab_dir, "bin"));
            var moab_lib_dir = Path.Combine(moab_dir, "bin").ToString();
            var moab_so_path = Path.Combine(moab_dir, "bin", moab_so);
            options.OutputDir = "../MOABSharp";

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Console.WriteLine("We're on Linux!");
                moab_so = "libMOAB.so";
                imesh_so = "libiMesh.so";
                moab_so_path = Path.Combine(moab_dir, "lib", moab_so);
                module.LibraryDirs.Add(Path.Combine(moab_dir, "lib"));
                moab_lib_dir = Path.Combine(moab_dir, "lib").ToString();
                options.OutputDir = "../MOABSharp.Linux";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                Console.WriteLine("MacOSX is not supported yet!");
            }
            else{
                Console.WriteLine(" By default, assuming we're on Windows!");
                if (false)
                {
                    // generate C++/CLI to support std::vector<>, but compiling vcxproj is not ready
                    options.GeneratorKind = CppSharp.Generators.GeneratorKind.CLI;
                    options.OutputDir = "../MOABSharp.CLI";
                }
            }
            
            if ( ! File.Exists(moab_so_path))
            {
                throw new IOException(string.Format($"{moab_so_path} does not exist!"));
            }

            module.LibraryDirs.Add(moab_lib_dir);
            module.Libraries.Add(moab_so);
            module.Libraries.Add(imesh_so);  
        }

        public void SetupPasses(Driver driver)
        {
            // those name are c++ name before translated into C# naming convection
            driver.Context.TranslationUnitPasses.RemovePrefix("MOAB_");
            driver.Context.TranslationUnitPasses.RemovePrefix("imesh_");
            driver.Context.TranslationUnitPasses.RemovePrefix("iBase_");
            driver.Context.TranslationUnitPasses.RemovePrefix("imoab_");

            //driver.ParserOptions.NoBuiltinIncludes = true;  // CppSharp.Parser.CLI

            //driver.Context.TypeMaps.FindTypeMap()

            // https://github.com/mono/CppSharp/blob/master/src/Generator/Types/TypeMapDatabase.cs
        }

        public void Preprocess(Driver driver, ASTContext ctx)
        {

            // IDD_MBCore   static method in `moab/Interface.hpp`
            //ctx.IgnoreClassWithName("Interface");
            //ctx.IgnoreHeadersWithName("Interface");

            //ctx.SetClassAsValueType("");  // by defaul all class except POD is treated as ref obj
            // SetClassAsOpaque()

            // these 2 API below use `type_info` RTTI not supported yet
            // this pass is before  C++ to C# function rename pass
            ctx.IgnoreClassMethodWithName("Interface", "query_interface_type");  // here use C++ names
            ctx.IgnoreClassMethodWithName("Interface", "release_interface_type");
            ctx.IgnoreClassMethodWithName("Core", "query_interface_type");  // full class name with namespace
            ctx.IgnoreClassMethodWithName("Core", "release_interface_type");

            ctx.IgnoreClassWithName("Types");  // c++ class name, not C# class name

            ctx.IgnoreClassMethodWithName("Core", "serial_read_tag");
            // ctx.SetNameOfClassMethod


            //ctx.IgnoreEnumWithMatchingItem("DUMMY_ENUM_VALUE");


            //ctx.SetNameOfEnumWithMatchingItem("MOAB_SCANCODE_UNKNOWN", "ScanCode");

            //ctx.GenerateEnumFromMacros("Endianness", "MOAB_(.*)_ENDIAN");
        }

        public void Postprocess(Driver driver, ASTContext ctx)
        {
            // ctx.SetNameOfEnumWithName("PIXELTYPE", "PixelType");
            ctx.SetClassBindName("TagInfo", "Tag");  // this is working
            // ctx.FindClass()   // ignore?
        }


    }

    static class Program
    {
        public static void Main(string[] args)
        {
            ConsoleDriver.Run(new MOAB());
        }
    }
    
}
