#define _LIBCPP_DISABLE_VISIBILITY_ANNOTATIONS
#define _LIBCPP_HIDE_FROM_ABI

#include <string>
#include <new>

template std::basic_string<char, std::char_traits<char>, std::allocator<char>>::basic_string() noexcept(true);
template std::basic_string<char, std::char_traits<char>, std::allocator<char>>::~basic_string() noexcept;
