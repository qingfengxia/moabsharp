using NUnit.Framework;
using MOAB;

using System.Runtime.InteropServices;
using System.Text;
using EntityHandle = System.UInt64;

namespace MOAB.Tests
{
    public class iMOABTests
    {
        [SetUp]
        public void Setup()
        {
            string[] args = new string[] {"moab_test"};

            iMOAB.Initialize(args);
        }

        [Test]
        public void Test1()
        {
            iMOAB.Finalize();
            Assert.Pass();
        }
    }

    public class TagTests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestTag()
        {

            var myMoabInstance = new MOAB.Moab.Core();
            MOAB.Moab.Tag geom_tag = new MOAB.Moab.Tag();
            
            string tag_name = "geom_tag_name";
            var rval = myMoabInstance.GetTagHandle<int>(tag_name, 1, MOAB.Moab.DataType.MB_TYPE_INTEGER,
                                        out geom_tag, MOAB.Moab.TagType.MB_TAG_SPARSE | MOAB.Moab.TagType.MB_TAG_CREAT, -1);
          
            int tag_data = 5;
            EntityHandle h = 0;  // h is output parameter
            myMoabInstance.SetTagData<int>( geom_tag, ref h, tag_data);

            int read_tag_data = 0;
            myMoabInstance.GetTagData<int>(geom_tag, ref h, out read_tag_data);
            Assert.AreEqual(tag_data, (int)read_tag_data);

        }
    }
}