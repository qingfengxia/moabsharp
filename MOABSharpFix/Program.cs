﻿using System;
using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Reflection;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace MOABSharpFix
{

    /// <summary>
    /// The CSharpSyntaxRewriter allows to rewrite the Syntax of a node
    /// </summary>
    public class AttributeStatementChanger : CSharpSyntaxRewriter
    {
        /// Visited for all AttributeListSyntax nodes
        /// The method replaces all PreviousAttribute attributes annotating a method by ReplacementAttribute attributes
        public override SyntaxNode VisitAttributeList(AttributeListSyntax node)
        {
            // If the parent is a MethodDeclaration (= the attribute annotes a method)
            if (node.Parent is MethodDeclarationSyntax &&
                // and if the attribute name is PreviousAttribute
                node.Attributes.Any(
                    currentAttribute => currentAttribute.Name.GetText().ToString() == "PreviousAttribute"))
            {
                // Return an alternate node that is injected instead of the current node
                return SyntaxFactory.AttributeList(
                                SyntaxFactory.SingletonSeparatedList(
                                SyntaxFactory.Attribute(SyntaxFactory.IdentifierName("ReplacementAttribute"),
                                    SyntaxFactory.AttributeArgumentList(
                                        SyntaxFactory.SeparatedList(new[]
                                        {
                                        SyntaxFactory.AttributeArgument(
                                            SyntaxFactory.LiteralExpression(
                                                SyntaxKind.StringLiteralExpression, SyntaxFactory.Literal(@"Sample"))
                                            )
                                        })))));
            }
            // Otherwise the node is left untouched
            return base.VisitAttributeList(node);
        }
    }

    public class ClassRemover : CSharpSyntaxRewriter
    {
        private readonly List<string> ClassesToRemove;
        public ClassRemover(List<string> clsList)
        {
            ClassesToRemove = clsList;
        }

        public override SyntaxNode VisitClassDeclaration(ClassDeclarationSyntax node)
        {
            foreach(var cls in ClassesToRemove)
            {
                if(node.Identifier.ToString() == cls)
                    return null;
            }
            return base.VisitClassDeclaration(node);
        }

    }

    public class ClassMethodRemover : CSharpSyntaxRewriter
    {
        private readonly List<string> MethodsToRemove;
        public ClassMethodRemover(List<string> methodList)
        {
            MethodsToRemove = methodList;
        }

        public override SyntaxNode VisitMethodDeclaration(MethodDeclarationSyntax node)
        {
            foreach (var m in MethodsToRemove)
            {
                char[] sep = new char[] { '.' };
                var t = m.Split(sep);
                
                if (node.Identifier.ToString() == t[t.Length -1])
                {
                    var cls = (BaseTypeDeclarationSyntax)(node.Parent);  // parent can be Struct
                    if(cls.Identifier.ToString() == t[t.Length - 2])
                        return null;
                }
            }
            return base.VisitMethodDeclaration(node);
        }

    }

    public class ClassOperatorRemover : CSharpSyntaxRewriter
    {
        private readonly List<string> OperatorsToRemove;
        public ClassOperatorRemover(List<string> methodList)
        {
            OperatorsToRemove = methodList;
        }

        public override SyntaxNode VisitConversionOperatorDeclaration(ConversionOperatorDeclarationSyntax node)
        {
            foreach (var m in OperatorsToRemove)
            {
                /*
                var opName = node.  // OperatorToken.ToString();
                if (opName.Contains(m))
                {
                    Console.WriteLine($"remove operator with name {opName}");
                    return null;
                }
                */
                char[] sep = new char[] { '.' };
                var t = m.Split(sep);
                var cls = (BaseTypeDeclarationSyntax)(node.Parent);
                var clsName = cls.Identifier.ToString();
                if ( clsName.Contains(t[0]))  // remove all operator in the class, only class name provided
                {
                    Console.WriteLine($"remove conversion operator in class {clsName}");
                    return null;
                }
                    
            }
            return base.VisitConversionOperatorDeclaration(node);
        }

    }

    public class Program
    {
        /// The method calling the Syntax Rewriter, using `MicroSoft.CodeAnalysis.CSharp.Workspace` assembly,needs lots installation
        /*
        private static async Task<bool> ModifySolutionUsingSyntaxRewriter(string solutionPath, CSharpSyntaxRewriter rewriter)
        {
            using (var workspace = MSBuildWorkspace.Create())
            {
                // Selects a Solution File
                var solution = await workspace.OpenSolutionAsync(solutionPath);
                // Iterates through every project
                foreach (var project in solution.Projects)
                {
                    // Iterates through every file
                    foreach (var document in project.Documents)
                    {
                        // Selects the syntax tree
                        var syntaxTree = await document.GetSyntaxTreeAsync();
                        var root = syntaxTree.GetRoot();

                        // Generates the syntax rewriter
                        root = rewriter.Visit(root);

                        // Exchanges the document in the solution by the newly generated document
                        solution = solution.WithDocumentSyntaxRoot(document.Id, root);
                    }
                }
                // applies the changes to the solution
                var result = workspace.TryApplyChanges(solution);
                return result;
            }
        }
        */

        static void Main(string[] args)
        {
            string repoPath = "..";  // if start in a project subfolder, so the repo dir is the parent folder
            
            if (!Directory.Exists(Path.Combine(repoPath, "MOABSharp")))
            {
                Console.WriteLine($"{Path.Combine(repoPath, "MOABSharp")} does not exist, assuming start the app by visual studio");
                repoPath = "../../../../";  // if run in visual studio, the current workdir is in "project/bin/Debug/netstandard2.0/"
            }
            
            string outputFolder = "MOABSharp";
            string outputFileName = "MOABSharp.Windows.cs";
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                outputFolder = "MOABSharp.Linux";
                outputFileName = "MOABSharp.Linux.cs";
            }
            string sourcePath = Path.Combine(repoPath, outputFolder);

            string sourceFileName = Path.Combine(sourcePath, "MOAB.cs");
            string outputFilePath = Path.Combine(sourcePath, outputFileName);
            if (!File.Exists(sourceFileName))
            {
                Console.WriteLine($"{sourcePath} does not exit, exit");
                return;
            }
            
            var sourceText = File.ReadAllText(sourceFileName);
            SyntaxTree sourceTree = CSharpSyntaxTree.ParseText(sourceText)
                             .WithFilePath(sourceFileName);

            Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();
            var clsList = new List<string>() {"Types"};
            var rewriter = new ClassRemover(clsList);

            // Inject some code, can be done in MOAB_Sharpen.cs with #

            var root = sourceTree.GetRoot();
            // Generates the syntax rewriter
            var newSource = rewriter.Visit(root);

            var methodList = new List<string>() { "Core.TagGetData", "Range.Print", "Range.EqualRange"};
            // this "Core.TagGetData" API will be added back with fixed in MOAB_sharpen.cs
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                methodList.Add("Interface.IDD_MBUnknown");  // todo: it is property not method, so not working yet
                methodList.Add("UnknownInterface.IDD_MBUnknown");  // it is property not method
            }
            var methodRemover = new ClassMethodRemover(methodList);
            newSource = methodRemover.Visit(newSource);

            /// ctor is treated as operator when ctor binding is not generated
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                var operatorList = new List<string>() { "FileOptions", "GeomTopoTool", "RangeInserter" };
                var operatorRemover = new ClassOperatorRemover(operatorList);
                newSource = operatorRemover.Visit(newSource);
            }

            // if we changed the tree we save a new file
            //if (newSource != sourceTree.GetRoot())
            {                    
                File.WriteAllText(outputFilePath, newSource.ToFullString());
                Console.WriteLine($"Successfully modified the {sourceFileName} and save to {outputFilePath}");
            }
        }
    }

}