using System;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Security;
using __CallingConvention = global::System.Runtime.InteropServices.CallingConvention;
using __IntPtr = global::System.IntPtr;

[assembly:InternalsVisibleTo("MOAB")]

namespace MOAB
{
    using EntityHandle = System.UInt64;   // type alias is only valid in the source file?
    /// depends on C++ build configuration and 64bit or 32bit, see MOAB's header <EntityHandle.hpp>
    using EntityID = System.Int64;  // <EntityHandle.hpp>  


    namespace Moab
    {

        /// renamed TagInfo to Tag not to confuse with C++ TagInfo class
        
        public unsafe partial class Tag
        {
            /// <summary> TagInfo handle contructor, point to nullptr/IntPtr.Zero </summary>
            public Tag()
                : this((void*)null)
            {
            }
        }
        

#if _WINDOWS  // this is a constant defiend in csproj xml file
        public unsafe partial class FileOptions
        {
            public partial struct __Internal
            {
[SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "_ZN4moab11FileOptionsC2EPKc", CallingConvention = __CallingConvention.Cdecl)]
                internal static extern void ctor(__IntPtr __instance, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(CppSharp.Runtime.UTF8Marshaller))] string option_string);
            }

            public FileOptions(string option_string)
            {
                __Instance = Marshal.AllocHGlobal(sizeof(global::MOAB.Moab.FileOptions.__Internal));
                __ownsNativeInstance = true;
                NativeToManagedMap[__Instance] = this;
                __Internal.ctor(__Instance, option_string);
            }
        }

        public unsafe partial class GeomTopoTool
        {
            public partial struct __Internal
            {
[SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "??0GeomTopoTool@moab@@QEAA@PEAVInterface@1@_N_K11@Z", CallingConvention = __CallingConvention.Cdecl)]
 internal static extern void ctor(__IntPtr __instance, __IntPtr impl, bool find_geoments, ulong modelRootSet, bool p_rootSets_vector, bool restore_rootSets);

            }

             /// <summary>
            /// <para>Constructor (creates a GTT object)\</para>
            /// <para>Construct a GeomTopoTool object and search for geometric EntitySets if they</para>
            /// <para>exist in the provided moab instance.</para>
            /// </summary>
            /// <param name="impl">MOAB instance the GeomTopoTool will operate on.</param>
            /// <param name="find_geoments">
            /// <para>if specified as True, geometric objects in the provided MOAB instance</para>
            /// <para>will be searched for and added to the GTT.</para>
            /// </param>
            /// <param name="modelRootSet">
            /// <para>the GTT will operate only on geometric EntitySets contained by this EntitySet.</para>
            /// <para>If unprovided, the default value for the modelRootSet is the MOAB instance's</para>
            /// <para>root set, which contains everything in the instance.</para>
            /// </param>
            /// <param name="p_rootSets_vector">
            /// <para>determines the storage datastructure used to relate geometric</para>
            /// <para>EntitySets to their OrientedBoundingBox (OBB) Tree roots. If</para>
            /// <para>set to true (default) a vector will be used to store the root</para>
            /// <para>sets along with an EntityHandle offset for fast lookup of the root</para>
            /// <para>sets. If set to false, then a map will be used to link geometric</para>
            /// <para>EntitySets (keys) to the OBB Tree root sets (values).</para>
            /// </param>
            /// <param name="restore_rootSets">
            /// <para>determines whether or not to restore the internal index that</para>
            /// <para>links geomSets to their corresponding OBB Root.  Only relevant if</para>
            /// <para>find_geoments is true. (default = true)</para>
            /// </param>
            public GeomTopoTool(global::MOAB.Moab.Interface impl, bool find_geoments, ulong modelRootSet, bool p_rootSets_vector, bool restore_rootSets)
            {
                __Instance = Marshal.AllocHGlobal(sizeof(global::MOAB.Moab.GeomTopoTool.__Internal));
                __ownsNativeInstance = true;
                NativeToManagedMap[__Instance] = this;
                var __arg0 = impl is null ? __IntPtr.Zero : impl.__Instance;
                __Internal.ctor(__Instance, __arg0, find_geoments, modelRootSet, p_rootSets_vector, restore_rootSets);
            }

        }

        public unsafe partial class Core
        {
            public partial struct __Internal
            {
                [SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "??0Core@moab@@QEAA@XZ", CallingConvention = __CallingConvention.Cdecl)]
                internal static extern __IntPtr ctor(__IntPtr __instance);
            }

            /// <summary>constructor</summary>
            public Core()
                : this((void*) null)
            {
                __Instance = Marshal.AllocHGlobal(sizeof(global::MOAB.Moab.Core.__Internal));
                __ownsNativeInstance = true;
                NativeToManagedMap[__Instance] = this;
                __Internal.ctor(__Instance);
                SetupVTables(GetType().FullName == "MOAB.Moab.Core");
            }
        }
#else
  // your *nix stuff
#endif

        public unsafe partial class Core
        {
            // here  `virtual TagGetData()` should be `override`, why not generate properly?
            // remove by MOABSharpFix and add back here with manual fix
            // remove these TagGetData() if CppSharp has fixed this

            public override global::MOAB.Moab.ErrorCode TagGetData(global::MOAB.Moab.Tag tag_handle, ref ulong entity_handles, int num_entities, __IntPtr tag_data)
            {
                var ___TagGetDataDelegate = __VTables.GetMethodDelegate<global::MOAB.Delegates.Func_MOAB_moab_ErrorCode___IntPtr___IntPtr_ulongPtr_int___IntPtr>(0, 81);
                var __arg0 = tag_handle is null ? __IntPtr.Zero : tag_handle.__Instance;
                fixed (ulong* __entity_handles1 = &entity_handles)
                {
                    var __arg1 = __entity_handles1;
                    var __ret = ___TagGetDataDelegate(__Instance, __arg0, __arg1, num_entities, tag_data);
                    return __ret;
                }
            }

            public override global::MOAB.Moab.ErrorCode TagGetData(global::MOAB.Moab.Tag tag_handle, global::MOAB.Moab.Range entity_handles, __IntPtr tag_data)
            {
                var ___TagGetData_1Delegate = __VTables.GetMethodDelegate<global::MOAB.Delegates.Func_MOAB_moab_ErrorCode___IntPtr___IntPtr___IntPtr___IntPtr>(0, 80);
                var __arg0 = tag_handle is null ? __IntPtr.Zero : tag_handle.__Instance;
                if (ReferenceEquals(entity_handles, null))
                    throw new global::System.ArgumentNullException("entity_handles", "Cannot be null because it is a C++ reference (&).");
                var __arg1 = entity_handles.__Instance;
                var __ret = ___TagGetData_1Delegate(__Instance, __arg0, __arg1, tag_data);
                return __ret;
            }


            /// <summary>
            /// Helper function to simplify call Core.TagGetHandle() from C# with `void *` as default tag data
            /// </summary>
            /// <param name="tag_name"></param>
            /// <param name="tag_size"></param>
            /// <param name="tag_dtype"></param>
            /// <param name="tag_handle"></param>
            /// <param name="tag_types"></param>
            /// <param name="default_value"> template/generic value type</param>
            /// <returns></returns>
            public Moab.ErrorCode GetTagHandle<T>(in string tag_name, int tag_size, Moab.DataType tag_dtype,
                out Moab.Tag tag_handle, Moab.TagType tag_types, in T default_value)
            {
                bool created = false; // it that only a out parameter?
                var ret = Moab.ErrorCode.MB_SUCCESS;
                tag_handle = new Tag();

                int size = Marshal.SizeOf(typeof(T));
                var arr = new byte[size];

                fixed (byte* tag_data_ptr = arr)
                {
                    Marshal.StructureToPtr(default_value, (IntPtr)tag_data_ptr, true);
                //var gchandle = GCHandle.Alloc(default_value);   GCHandle.ToIntPtr(gchandle)
                ret = TagGetHandle1(tag_name, tag_size, tag_dtype,
                                    ref tag_handle, (uint)(tag_types), (IntPtr)tag_data_ptr, ref created);
                    //gchandle.Free();
                }
                
                return ret;
            }

            public Moab.ErrorCode GetTagHandle(in string tag_name, int tag_size, Moab.DataType tag_dtype,
                out Moab.Tag tag_handle, Moab.TagType tag_types)
            {
                bool created = false; // it that only a out parameter?
                tag_handle = new Tag(); // Tag.__CreateInstance(IntPtr.Zero);
                return TagGetHandle1(tag_name, tag_size, tag_dtype,
                   ref tag_handle, (uint)(tag_types), IntPtr.Zero, ref created);
            }

            /// <summary>
            ///  instead of modify the generated API (should be deleted), add new API 
            ///  this function needs extra care, if UnitTest failed, check and get the latest `___TagGetHandleDelegate`
            ///  from the generated TagGetHandle()
            /// </summary>
            /// <param name="name"></param>
            /// <param name="size"></param>
            /// <param name="type"></param>
            /// <param name="tag_handle"></param>
            /// <param name="flags"></param>
            /// <param name="default_value"></param>
            /// <param name="created"></param>
            /// <returns></returns>
            private global::MOAB.Moab.ErrorCode TagGetHandle1(string name, int size, global::MOAB.Moab.DataType type, ref global::MOAB.Moab.Tag tag_handle, uint flags, IntPtr default_value, ref bool created)
            {
                var ___TagGetHandleDelegate = __VTables.GetMethodDelegate<global::MOAB.Delegates.Func_MOAB_moab_ErrorCode___IntPtr__MarshalAs_UnmanagedType_CustomMarshaler__MarshalTypeRef___typeof_CppSharp_Runtime_UTF8Marshaller____string_int_MOAB_moab_DataType___IntPtr_uint___IntPtr_boolPtr>(0, 70);
                //if (ReferenceEquals(tag_handle, null))
                //    throw new global::System.ArgumentNullException("tag_handle", "Cannot be null because it is a C++ reference (&).");
                var ____arg3 = tag_handle.__Instance;
                var __arg3 = new IntPtr(&____arg3);
                //var __arg3 = new IntPtr();
                fixed (bool* __created6 = &created)
                {
                    var __arg6 = __created6;
                    var __ret = ___TagGetHandleDelegate(__Instance, name, size, type, __arg3, flags, default_value, __arg6);
                    tag_handle = Tag.__CreateInstance(____arg3, false);
                    return __ret;
                }
            }

            /// <summary>
            /// helper for `moab::Core::tag_set_data()` passing opaque pointer (C++ void*) IntPtr in C#
            /// https://stackoverflow.com/questions/17339928/c-sharp-how-to-convert-object-to-intptr-and-back
            /// </summary>
            /// <param name="tag_handle"></param>
            /// <param name="entity_handle"> this is input parameter, but can not be </param>
            /// <param name="tag_data"> template value or string, Array<valueType> object type </param>
            /// <returns></returns>
            public Moab.ErrorCode SetTagData<T>(Moab.Tag tag_handle, ref EntityHandle entity_handle, in T tag_data)
            {
                int numEntity = 1;
                Moab.ErrorCode ret = Moab.ErrorCode.MB_SUCCESS;
                if (typeof(T).IsValueType)  // where T: struct
                {

                    int size = Marshal.SizeOf(typeof(T));
                    var arr = new byte[size];

                    fixed (byte* tag_data_ptr = arr)
                    {
                        Marshal.StructureToPtr(tag_data, (IntPtr)tag_data_ptr, true);
                        ret = TagSetData(tag_handle, ref entity_handle, numEntity, (IntPtr)tag_data_ptr);
                    }
                }
                else  // only support string type, and value type array
                {
                    if (typeof(T) == typeof(String))
                    {
                        string s = tag_data.ToString();
                        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(s);  // trailing 
                        if(bytes[bytes.Length-1] != 0)
                        {
                            throw new Exception("no trailing NUL for c-str(const char*)");
                        }
                        fixed (byte* tag_data_ptr = bytes)
                        {
                            ret = TagSetData(tag_handle, ref entity_handle, numEntity, (IntPtr)tag_data_ptr);
                        }

                    }
                    //else if (typeof(T) is Array)
                    else
                    {
                        throw new NotSupportedException("Set Tag data only support string and value types");
                    }
                    //ret = TagSetData(tag_handle, ref entity_handle, numEntity, (IntPtr)tag_data_ptr);
                }

                return ret;
            }

            /// <summary>
            ///  ref should be not necessary for tag_handle
            /// </summary>
            /// <param name="tag_handle"></param>
            /// <param name="entity_handle"></param>
            /// <param name="tag_data"></param>
            /// <returns></returns>
            public unsafe Moab.ErrorCode GetTagData<T>(Moab.Tag tag_handle, ref EntityHandle entity_handle, out T tag_data) where T: struct
            {
                int numEntity = 1;
                Moab.ErrorCode ret = Moab.ErrorCode.MB_SUCCESS;

                int size = Marshal.SizeOf(typeof(T));
                var arr = new byte[size];
                tag_data = default(T);

                fixed (byte* tag_data_ptr = arr)
                {
                    ret = TagGetData(tag_handle, ref entity_handle, numEntity, (IntPtr)tag_data_ptr);
                    tag_data = (T)Marshal.PtrToStructure((IntPtr)tag_data_ptr, typeof(T));
                }

                return ret;
            }

            public unsafe Moab.ErrorCode GetTagOpaqueData<T>(Moab.Tag tag_handle, ref EntityHandle entity_handle, out T tag_data) where T: class
            {
                int numEntity = 1;
                Moab.ErrorCode ret = Moab.ErrorCode.MB_SUCCESS;
                if (typeof(T) == typeof(String))
                {
                    // should use the specialization 
                }
                /*  not completed code,  opaque, must be marschal into byte array, its member __Instance is protected  */
                var gchandle = GCHandle.Alloc(typeof(T));  // GCHandleType::Pinned    Type dtype
                ret = TagGetData(tag_handle, ref entity_handle, numEntity, GCHandle.ToIntPtr(gchandle));
                tag_data = (T)gchandle.Target;
                gchandle.Free();
                
                return ret;
            }

            public unsafe Moab.ErrorCode GetTagData<T>(Moab.Tag tag_handle, ref EntityHandle entity_handle, out T[] tag_data) where T : struct
            {
                int numEntity = 1;
                Moab.ErrorCode ret = Moab.ErrorCode.MB_SUCCESS;

                throw new NotSupportedException("Set Tag data only support string and value types");

                /* opaque, must be marschal into byte array, its member __Instance is protected  
                var gchandle = GCHandle.Alloc(typeof(T));  // GCHandleType::Pinned    Type dtype
                ret = TagGetData(tag_handle, ref entity_handle, numEntity, GCHandle.ToIntPtr(gchandle));
                tag_data = (T)gchandle.Target;
                gchandle.Free();
                */

                return ret;
            }

            public unsafe Moab.ErrorCode GetTagData(Moab.Tag tag_handle, ref EntityHandle entity_handle, out string tag_data)
            {
                int numEntity = 1;  // var length -1?
                var cstr_p = new IntPtr();
                var ret = TagGetData(tag_handle, ref entity_handle, numEntity, cstr_p);
                //tag_data = Marshal.PtrToStringUTF8(cstr_p);  // dotnet 5 API
                tag_data = Marshal.PtrToStringAnsi(cstr_p);
                return ret;
            }
        }


    }
    public static class Constants
    {
        /* MATERIAL_SET_TAG_NAME tag:
     * Represents sets of elements having a common material (corresponds to
     * element blocks in ExodusII)
     * size = sizeof(int)
     * type = int
     * value = integer id for this set (block id from ExodusII)
     * default value = -1
     */
        public const string MATERIAL_SET_TAG_NAME = "MATERIAL_SET";

        /* DIRICHLET_SET_TAG_NAME tag:
         * Represents dirichlet-type boundary condition, usually contains only mesh vertices
         * (corresponds to nodesets in ExodusII)
         * size = sizeof(int)
         * type = int
         * value = integer id for this set (nodeset id from ExodusII)
         * default value = -1
         */
        public const string DIRICHLET_SET_TAG_NAME = "DIRICHLET_SET";

        /* NEUMANN_SET_TAG_NAME  tag:
         * Represents neumann-type boundary condition, usually contains elements with dimension
         * one lower than those found in material sets (i.e. edges in FE quad/tri models, quads/tris
         * in FE hex/tet models) (corresponds to sidesets in ExodusII)
         * size = sizeof(int)
         * type = int
         * value = integer id for this set (sideset id from ExodusII)
         * default value = -1
         */
        public const string NEUMANN_SET_TAG_NAME = "NEUMANN_SET";

        /* HAS_MID_NODES_TAG_NAM tag:
         * Flags telling whether elements in a given set have mid-(edge, face, region) vertices/nodes;
         * index 0 is a place holder, so this datum can be indexed by dimension, e.g. has_mid_nodes[dim]
         * indicates whether mesh entities of dimension dim have mid nodes
         * size = 4*sizeof(int)
         * type = int[4]
         * value = 1 (has mid nodes), 0 (does not have mid nodes)
         * default value = [-1, -1, -1, -1]
         */
        public const string HAS_MID_NODES_TAG_NAME = "HAS_MID_NODES";

        /* GEOM_DIMENSION tag:
         * Represents entities "owned" by a given topological entity in a geometric model
         * size = sizeof(int)
         * type = int
         * value = dimension of geom entity
         * default value = -1
         */
        public const string GEOM_DIMENSION_TAG_NAME = "GEOM_DIMENSION";

        /* MESH_TRANSFORM tag:
         * Represents homogeneous transform to be applied to mesh; used in ExodusII writer to apply
         * transform before writing nodal coordinates
         * size = 16*sizeof(double)
         * type = double[16]
         * value = 4x4 homogenous transform matrix
         */
        public const string MESH_TRANSFORM_TAG_NAME = "MESH_TRANSFORM";

        /* GLOBAL_ID tag:
         * Represents global id of entities (sets or mesh entities); this id is different than the id
         * embedded in the entity handle
         * size = sizeof(int)
         * type = int
         * value = global id
         * default value = 0 // not -1 to allow gids stored in unsigned data types
         */
        public const string GLOBAL_ID_TAG_NAME = "GLOBAL_ID";

        /* CATEGORY tag:
         * String name indicating generic "category" if the entity to which it is assigned (usually
         * sets); used e.g. to indicate a set represents geometric vertex/edge/face/region,
         * dual surface/curve, etc.
         * size = CATEGORY_TAG_NAME_LENGTH (defined below)
         * type = char[CATEGORY_TAG_NAME_LENGTH]
         * value = NULL-terminated string denoting category name
         */
        public const string CATEGORY_TAG_NAME = "CATEGORY";
        public const int CATEGORY_TAG_SIZE = 32;

        /* NAME tag:
         * A fixed length NULL-padded string containing a name.
         * All values should be assumed to be of type char[NAME_TAG_SIZE].
         * The string need not be null terminated.  All values used for
         * storing or searching for a value must be padded with '\0' chars.
         */
        public const string NAME_TAG_NAME = "NAME";
        public const int NAME_TAG_SIZE = 32;

        /* BLOCK_HEADER: tag
         * A fixex lenght tag containg block header data
         * BlockColor, MaterialId and BlockDimension
         */
        public const string BLOCK_HEADER = "BLOCK_HEADER";

        /* BLOCK_ATTRIBUTES: tag
         * A varible lenght tag of doubles
         * Tag contains attributes set to BlockSet in cubit file
         */
        public const string BLOCK_ATTRIBUTES = "BLOCK_ATTRIBUTES";
    }

    public unsafe partial class iMOAB
    {
        /// helper function to call `iMOAB.Initialize(int argc, sbyte** argv)`
        /// it should be safe for ASCII string input, from ASCII Bytes (max 127) cast to SBytes, c-string ending with \0
        /// todo: ASCII to UTF8 encoding
        public static int Initialize(string[] args)
        {
            var argv = (sbyte**)Marshal.AllocHGlobal(args.Length * System.IntPtr.Size).ToPointer();
            var maxCstrLength = 256;

            for(int i=0; i<args.Length; i++)
            {
                var astr = System.Text.Encoding.ASCII.GetBytes(args[i]);
                argv[i] = (sbyte*)Marshal.AllocHGlobal(maxCstrLength).ToPointer(); 
                // copy and append c-str ending \0
                // Namespace:     System.Runtime.InteropServices  public static IntPtr StringToHGlobalAnsi (string? s);
                int j=0;
                for(; j<astr.Length && j<maxCstrLength-1; j++)
                {
                    (argv[i])[j] = (sbyte)astr[j];
                }
                (argv[i])[j] = 0;
            }

            var ret = iMOAB.Initialize(args.Length, argv);

            // Always free the unmanaged string.
            for(int i=0; i<args.Length && argv[i] != null; i++)
            {
                Marshal.FreeHGlobal((System.IntPtr)argv[i]);
            }
            Marshal.FreeHGlobal((System.IntPtr)argv);
            return ret;
         }
    };
}