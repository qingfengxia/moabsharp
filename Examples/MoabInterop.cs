
using System;
using System.Runtime.InteropServices;

using System.Security;
using __CallingConvention = global::System.Runtime.InteropServices.CallingConvention;

//  https://docs.microsoft.com/en-us/dotnet/standard/native-interop/pinvoke

namespace M
{
    public unsafe static class MOAB
    {

        [SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "imoab_initialize_", CallingConvention = __CallingConvention.Cdecl)]
        public static extern int Initialize(int argc, sbyte** argv);

        [SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "imoab_finalize_", CallingConvention = __CallingConvention.Cdecl)]
        public static extern int Finalize();
        
        public unsafe static void Test()
        {

            // https://stackoverflow.com/questions/12494610/how-to-marshalling-a-function-with-char-param-in-c-sharp
            var argc = 1;
            var maxCstrLength = 100;
            var argv = (sbyte**)Marshal.AllocHGlobal(argc * IntPtr.Size).ToPointer();
            //sbyte[] appName = "testapp";
            for (int i = 0; i < argc; ++i)
            {
                argv[i] = null;
                //argv[i] =  (sbyte*)Marshal.AllocHGlobal(maxCstrLength).ToPointer();  // Cannot implicitly convert type 'string' to 'sbyte[]'
            }

            MOAB.Initialize(argc, argv);
            MOAB.Finalize();

            // need to free memory
            // if (argv != null)
            // {
            //     for (int i = 0; i < argc && argv[i] != null; ++i)
            //         Marshal.FreeHGlobal(IntPtr(argv[i]));
            //     Marshal.FreeHGlobal(IntPtr(argv));
            // }
        }
    }

}

