#!/usr/bin/env dotnet-script
/*
https://bitbucket.org/fathomteam/moab/src/master/examples/beginner/mbex1.cpp

why mbcore.create_vertices
EntityHandle is a macro, so using alias is used here
*/

//  #! "netcoreapp3.1"

//#r "C:/Users/qxia/.nuget/packages/cppsharp/0.11.2/runtimes/win-x64/lib/netcoreapp3.1/CppSharp.Runtime.dll"
//#r "../MOABSharp/bin/x64/Debug/netcoreapp3.1/MOABSharp.dll"

#r "D:\MyRepo\DAGMC_win_bld\CppSharp\bin\Release_x64\CppSharp.Runtime.dll"
#r "../MOABSharp/bin/Debug/netcoreapp3.1/MOABSharp.dll"
// dotnet 3.1 does not have x64 in path

#if Linux
Console.WriteLine("Built on Linux!");

#elif OSX
Console.WriteLine("Built on macOS!");

#elif Windows
Console.WriteLine("Run on Windows!");
#endif


using MOAB;
using MOAB.Moab;

using EntityHandle = System.UInt64;  // C# module can not export type alias, so must redo it here

void MB_CHK_SET_ERR(ErrorCode rval,  string message)
{
    if (rval != ErrorCode.MB_SUCCESS) {
        System.Console.WriteLine("ErrorCode {0} wth message {1}", rval, message);
    }
}

int example1()
{
    //string[] args = new string[] {"moab_test"};
    //MOAB.iMOAB.Initialize(args);
    //MOAB.iMOAB.Finalize();

    ErrorCode rval = ErrorCode.MB_SUCCESS;
    Console.WriteLine("ErrorCode enum print as {0} ", rval.ToString());

    // MOAB functionality is accessed through an instance of the
    // Interface class:
    Core mbcore = new Core();

    // ***************************
    // *   Create the vertexes   *
    // ***************************

    // We are going to create 27 vertexes which will be used to define 8
    // hexahedron cells. First, we have to create an array to store the
    // coordinates of each vertex.

    const uint NUMVTX = 27;  // The number of vertexes
    const uint NUMHEX = 8;   // The number of hexahedrons

    // This double array stores the x, y, z coordinate of each vertex.
    double[] vertex_coords =  new double[] {
        0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 1, 0, 1, 1, 0, 2, 1, 0, 0, 2, 0, 1, 2, 0, 2, 2, 0,
        0, 0, 1, 1, 0, 1, 2, 0, 1, 0, 1, 1, 1, 1, 1, 2, 1, 1, 0, 2, 1, 1, 2, 1, 2, 2, 1,
        0, 0, 2, 1, 0, 2, 2, 0, 2, 0, 1, 2, 1, 1, 2, 2, 1, 2, 0, 2, 2, 1, 2, 2, 2, 2, 2
    };

    // Create the vertexes and store their entity handles in the
    // vertex_handles range. In MOAB, entities are defined using Entity
    // Handles (type EntityHandle). An entity handle is a unique
    // integer that is used to identify/refer to specific entities (such
    // as vertexes, edges, hexahedrons, etc...) on the mesh. MOAB
    // guarantees that when multiple entities are created at the same
    // time, as in the create_vertices call below, that those entities
    // get adjacent handles. Thus, the second of the 27 vertexes we are
    // creating will have an entity handle that is 1 greater than the
    // handle of the first vertex. A range (type Range) is a
    // container which stores entity handles. Of course, sets of handles
    // can also be stored in vectors or arrays, but ranges are much more
    // memory efficient, so use them when possible!
    Range vertex_handles = new Range();
    rval = mbcore.CreateVertices(ref vertex_coords[0], (int)NUMVTX, vertex_handles );
    MB_CHK_SET_ERR( rval, "create_vertices failed" );

    // You can print out a range to see what elements it contains:
    //Console.WriteLine( "Created 27 vertex entities: {0}", vertex_handles);  // ToString() not impl

    // ******************************
    // *   Create the Hexahedrons   *
    // ******************************

    // The conn array stores the connectivity for each hex. It defines
    // which 8 vertexes connect to define a single hexahedron. The loop
    // below adds the handle of the first vertex (stored in
    // first_vertex_handle) to conn thereby ensuring that the entries of
    // conn are actual entity handles. This only works because MOAB
    // guarantees that entities (such as our vertexes) created at once
    // have adjacent entity handles.
    EntityHandle[,] conn = new EntityHandle[,] { { 0, 1, 4, 3, 9, 10, 13, 12 }, { 1, 2, 5, 4, 10, 11, 14, 13 },
                                           { 3, 4, 7, 6, 12, 13, 16, 15 },     { 4, 5, 8, 7, 13, 14, 17, 16 },
                                           { 9, 10, 13, 12, 18, 19, 22, 21 },  { 10, 11, 14, 13, 19, 20, 23, 22 },
                                           { 12, 13, 16, 15, 21, 22, 25, 24 }, { 13, 14, 17, 16, 22, 23, 26, 25 } };

    // Lets get the handle for the first vertex. Note that we can use
    // the square brackets operator on ranges just like vectors or
    // arrays:
    EntityHandle first_vertex_handle = vertex_handles[0];

    for( uint i = 0; i < NUMHEX; ++i )
    {
        for( uint j = 0; j < 8; ++j )
        {
            // Add first_vertex_handle to each element of conn. This ensures
            // that the handles are specified properly (i.e. when
            // first_vertex_handle > 0)
            conn[i,j] = conn[i,j] + first_vertex_handle;
        }
    }

    // Now that the connectivity of each hex has been defined, we can
    // create each hex using a call to the create_element method which
    // gives back an entity handle for the hex that was created. We'll
    // then insert that entity into a range:
    Range hexahedron_handles = new Range();
    ulong element = 0;
    for( uint i = 0; i < NUMHEX; ++i )
    {
        rval = mbcore.CreateElement( MOAB.Moab.EntityType.MBHEX, ref conn[i, 0], 8, ref element );
        MB_CHK_SET_ERR( rval, "create_element failed");

        hexahedron_handles.Insert( element );
    }

    // Let's see what entities we just created:
    //Console.WriteLine( "Created HEX8 entities: {}",  hexahedron_handles);
    Console.WriteLine( "Created HEX8 entities");

    // ***************************
    // *   Write Mesh to Files   *
    // ***************************

    // Now that we've created this amazing mesh, we can write it out to
    // a file. Since MOAB can write out to a variety of standard file
    // formats, you can quickly visualize and manipulate your mesh using
    // standard tools, such as VisIt.

    // In these examples, we will stick to using the VTK file format
    // because it is text based and will work whether or not you've got
    // HDF5, NETCDF, etc... installed and is a fairly standard file
    // format so a lot of tools work with it out of the box.

    EntityHandle h = 0;  /// to mimic nullptr  for  "EntityHandle*" in C++

    var outputFlile = "mbex1.vtk";  // vtk h5m is working
    // todo: simplify this API?
    rval = mbcore.WriteFile(outputFlile, null, null, ref h, 0, null, 0);
    MB_CHK_SET_ERR( rval, $"WriteFile {outputFlile} failed" );
    Console.WriteLine("save mesh as " + outputFlile);


    outputFlile = "mbex1.h5m";  // vtk h5m is working
    // todo: simplify this API?
    rval = mbcore.WriteFile(outputFlile, null, null, ref h, 0, null, 0);
    MB_CHK_SET_ERR( rval, $"WriteFile {outputFlile} failed" );
    Console.WriteLine("save mesh as " + outputFlile);

    return 0;

}

example1();