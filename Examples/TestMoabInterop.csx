#!/usr/bin/env dotnet-script

//  #! "netcoreapp2.1"

// no Main, no namespace 

#r "./bin/Debug/netcoreapp3.1/MOAB.Tests.dll"


using System;
using System.Runtime.InteropServices;

using MOAB.Tests;


static class CLib
{
    // Import the libc shared library and define the method
    // corresponding to the native function.
    [DllImport("libc.so.6")]
    internal static extern int getpid();
}

int pid = CLib.getpid();
Console.WriteLine(pid);

M.MOAB.Test();