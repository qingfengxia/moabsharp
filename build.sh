#!/bin/bash

echo "CppSharp_DIR=$CppSharp_DIR"

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

#setCppSharp_DIR should have been done before running this script
# for gitbash
#export PATH=$PATH:${CppSharp_DIR}
# for Linux
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${CppSharp_DIR}

#
cd MOABSharp
echo "MOAB_DIR=$MOAB_DIR"

# clean, otherwise, modified CppSharp dll will not been copied to project build folder
dotnet clean

# must remove the auto generated file before re-generation or compiling after manually fix the bindings
if [ "${machine}" == "Windows" && -f MOABSharp/MOAB.cs ]; then     rm MOABSharp/MOAB.cs; fi
if [ "${machine}" == "Linux" && -f MOABSharp.Linux/MOAB.cs ]; then rm MOABSharp.Linux/MOAB.cs; fi
######################
# 3 steps: 1) generate 2) manually rename and fix 3) compile
######################

echo "Step 1: generate C# binding for MOAB by CppSharp"
cd MOAB.Gen && dotnet build && dotnet run >moab_gen.log  &&  cd ..
# this will generate MOAB.cs  Std.cs  Std-symbols.cpp, MOAB-Symbols.cpp  into ../MOABSharp on Windows

echo "Step 2: fix C# binding for MOAB by CppSharp"
cd MOABSharpFix && dotnet build && dotnet run  &&  cd ..

if [ "${machine}" == "Windows" && -f MOABSharp/MOABSharp.Windows.cs || "${machine}" == "Linux" && -f MOABSharp.Linux/MOABSharp.Linux.cs ]; then
    echo "step 3: build the C# binding for MOAB by CppSharp"
    if [ "${machine}" == "Windows" ]; then
    echo "must run in visual studio command line prompt"
    cd MOABSharp && dotnet build > moab_build.log  && cd ..
    fi

    # in the future version of CppSharp, this Std-symbols.cpp can be compiled by CppSharp without user intervention
    if [ "${machine}" == "Linux" ]; then
    g++ -shared Std-symbols.cpp -I $MOAB_DIR/include -L $MOAB_DIR/lib -lMOAB -liMesh -o libStd-symbols.so
    # failed, but does not needed
    g++ -shared MOAB-symbols.cpp -I $MOAB_DIR/include -L $MOAB_DIR/lib -lMOAB -liMesh -o libMOAB-symbols.so
    cd MOABSharp.Linux && dotnet build >moab_build.log  && cd ..
    fi
else
    echo "failed to find the fixed binding source file"
fi

# unit test, csproj file has conditional ItemGroup to select MOABSharp.dll on each platform
dotnet build MOAB.Tests  && dotnet test MOAB.Tests

# exmaple
dotnet-script Examples/MOAB_example.csx