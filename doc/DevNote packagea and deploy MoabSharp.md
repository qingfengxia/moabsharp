## DevNote packagea and deploy MoabSharp

### files to deploy
+ CppSharp.Runtime.dll
+ C# bindings’s managed shared library *.dll:  MOABSharp.dll
+ Std-symbols.so/dll/.dylib, `<target_lib>-symbols.dll`
+ MOAB c++ native shared library `MOAB.dll/libMOAB.so`, and their dependent such as VTK, HDF5

make `libMOAB.so` loadable at runtime is user's responsibility, via appending to LD_LIBRARY_PATH on Linux.
MOABSharp.dll + CppSharp.Runtime.dll can be put to dotnet app's binary folder, or dynamic loaded by IronPython.

dotnet assemblies `CppSharp.Runtime.dll`, `<Target_lib>Sharp.dll` (this name is up to user)
with native C++ libraries `Std-symbols.dll`, `<target_lib>-symbols.dll`, `<Target_lib>.dll`

version of `CppSharp.Runtime.dll`, `<Target_lib>Sharp.dll` must matching?


### Packaging strategy

Packing can be achived by MOABSharp.csproj xml file for Windows.
target on `netstandard 2.0` covering all dotnet runtimes.

There is no such urgent need to pack for Linux, skip for the moment.

To targets on more dotnet
`<TargetFrameworks>net472;netstandard2.0</TargetFrameworks>`


### native dll file: MOAB.dll

```xml
    <Content Include="$(MOAB_DIR)/bin/MOAB.dll" copyToOutput="true">
      <IncludeInPackage>true</IncludeInPackage>
      <CopyToOutput>true</CopyToOutput>
      <BuildAction>Content</BuildAction>
      <copyToOutput>true</copyToOutput>
      <CopyToOutputDirectory>Always</CopyToOutputDirectory>
    </Content>
```
This xml snippet will copy the include content to the output folder. These MOAB dll files will also be copied to the target build output if refered by another project, e.g. Damgc_Toolbox C# project) and also packed into nuget package


### ldd equivalent: Dependencies

The modern version of dependency walker written in C#

https://github.com/lucasg/Dependencies

LAPPACK is from Anaconda3 `mkl_sequential.dll`

NOTE: why hdf5 dll is not listed as dependencies?

### MOAB dep: HDF5 dll files

`C:\Program Files\HDF_Group\HDF5\1.12.0\bin>dumpbin /dependents hdf5_hl_cpp.dll`
gives:

    hdf5_hl.dll
    VCRUNTIME140.dll
    api-ms-win-crt-runtime-l1-1-0.dll
    api-ms-win-crt-heap-l1-1-0.dll
    KERNEL32.dll
VC runtime `VCRUNTIME140.dll` in the same folder `C:\Program Files\HDF_Group\HDF5\1.12.0\bin`  should have been available %SYSTEM32%


MOABSharp.csproj xml file

```xml
	<Content Include="C:\Program Files\HDF_Group\HDF5\1.12.0\bin\hdf5.dll" copyToOutput="true">
		<IncludeInPackage>true</IncludeInPackage>
		<CopyToOutput>true</CopyToOutput>
		<BuildAction>Content</BuildAction>
		<copyToOutput>true</copyToOutput>
		<CopyToOutputDirectory>Always</CopyToOutputDirectory>
	</Content>
```

#### Note: HDF5 dll file path is hardcoded
  The path of hdf5*.dll is hardcoded, needs to be updated if new hdf5 is installed to compiled MOAB


### Windows DLL search rule:

https://docs.microsoft.com/en-us/windows/win32/dlls/dynamic-link-library-search-order

SafeDllSearchMode 

1. The directory from which the application is loaded
2. The system directory
3. The 16-bit system directory
4. The Windows directory
5. The current directory (same as 1 unless otherwise specified)
6. The directories that are listed in the PATH environment variable



https://www.contextis.com/en/blog/dll-search-order-hijacking

Best practices have to be followed by software developers, as well. For example, when an application is loading modules in run-time, it is recommended to specify an absolute path for modules provided to LoadLibrary and LoadLibraryEx APIs or use the API SetDllDirectory to specifically set the path from where the Windows loader will load the provided module. By doing this, the normal search order is not being looked up. One more recommendation in accordance to best practises would be to implement integrity checks for the loaded modules via [Application Manifests](https://docs.microsoft.com/en-us/windows/win32/sbscs/application-manifests). By doing this the application will prevent the load of rogue DLLs and potentially inform the users of identified anomalies.



### CI to generate and upload nuget package

`dotnet pack -v normal -c Release --include-symbols --include-source -p:PackageVersion=$VERSION -o nupkg src/$PROJECT_NAME/$PROJECT_NAME.*proj`

`dotnet publish -r win-x64 -c Release /p:PublishSingleFile=true /p:PublishTrimmed=true`
`dotnet new classlib --output MOABSharp `  

