
## MOAB TagInfo Handle passing

`typedef TagInfo* Tag` 

TagInfo is the base class for a few derived classes, which are not genearted into C#

TagInfo in C# is class (reference type),  just like `TagHandlePrivate` is a wrapper of opaque c++ class' pointer

TODO: renamed to TagHandle/Tag not to confuse with C++ TagInfo class

```c#
        public unsafe partial class TagInfo
        {
            public partial struct __Internal
            {
            }
            
            public __IntPtr __Instance { get; protected set; }

            internal static readonly global::System.Collections.Concurrent.ConcurrentDictionary<IntPtr, global::MOAB.Moab.TagInfo> NativeToManagedMap = new global::System.Collections.Concurrent.ConcurrentDictionary<IntPtr, global::MOAB.Moab.TagInfo>();

            protected bool __ownsNativeInstance;
```



### Create tag in C#

TagInfo has no public ctor in the CppSharp generated C# binding, so a public contructor is added into `MOAB_sharpen.cs`.

```
/// Tag&  --> TagInfo** it is an output parameter, it can be zero
ErrorCode Core::tag_get_handle( const char* name, int size, DataType data_type, Tag& tag_handle, unsigned flags,
                                const void* default_value, bool* created )

```

`Tag&`  is  `(TagInfo*)&` is used only 3 times `TagGetHandle()` which may create a new tag.
The CppSharp generated API `TagGetHandle()` does not work properly. 

All C# function with `Tag&` as parameter must be `ref or out` parameter type, instead of `TagInfo` in `TagGetHandle()`

All other function use Tag as parameter,  `TagInfo*` is mapped to `TagHandle` this reference class type, correctly

Solution:  instead of modify the generated `Core.TagGetHandle()` which will be removed in postprocessing passes, new API `Core.GetTagHandle()` is added into `MOAB_sharpen.cs` 


### Pass C# object as opaque c++ class pointer in `TagSetData`

Core.cpp, `sequenceManager` is private, no change to rewrite this function in C#
```cpp
//! return the tag data for a given EntityHandle and Tag
ErrorCode Core::tag_get_data( const Tag tag_handle, const EntityHandle* entity_handles, int num_entities,
                              void* tag_data ) const
{
    assert( valid_tag_handle( tag_handle ) );
    CHECK_MESH_NULL
    return tag_handle->get_data( sequenceManager, mError, entity_handles, num_entities, tag_data );
}

//! set the data  for given EntityHandles and Tag
ErrorCode Core::tag_set_data( Tag tag_handle, const EntityHandle* entity_handles, int num_entities,
                              const void* tag_data )
{
    assert( valid_tag_handle( tag_handle ) );
    CHECK_MESH_NULL
    return tag_handle->set_data( sequenceManager, mError, entity_handles, num_entities, tag_data );
}

```

Solution:  new generic APIs `Core.SetTagData<T>()` and `Core.GetTagData<T>()`have been added into into `MOAB_sharpen.cs`.
