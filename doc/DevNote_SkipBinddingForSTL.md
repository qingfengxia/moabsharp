## DevNote_BindingGenerationOnWindows

Summary:
+ dllexport is not enforced in MOAB cause lots of trouble
+ `std::vector` is not 

### `std::vector<>` in C++ should be map to `List<>`, but not supported in C#

https://github.com/mono/CppSharp/issues/1590

> You can try adding a new typemap to https://github.com/mono/CppSharp/blob/main/src/Generator/Types/Std/Stdlib.CSharp.cs.

Something like:
```c#
[TypeMap("std::vector", GeneratorKind = GeneratorKind.CSharp)]
public partial class Vector : TypeMap
{
    public override bool IsIgnored { get { return true; } }
}
```

### STL container is supported by C++/CLI generator only

CppSharp has an exmaple of interface with `std::vector`, using `Microsoft.VisualC.STLCLR.dll`  v4.0 from GAC
dotnet core does not look into GAC
`C:\Windows\Microsoft.NET\assembly\GAC_MSIL\Microsoft.VisualC.STLCLR\v4.0_2.0.0.0__b03f5f7f11d50a3a`

see how to port c++/CLI project from visual stuio to dotnet: 
https://devblogs.microsoft.com/cppblog/porting-a-c-cli-project-to-net-core/

https://docs.microsoft.com/en-us/dotnet/api/microsoft.visualc.stlclr?view=netframework-4.8
C++/CLI is supported on dotent core, but must be built by visual studio
https://devblogs.microsoft.com/cppblog/an-update-on-cpp-cli-and-dotnet-core/


mono has Visualbasic, not sure visual c++, but dotnet core do have visual c++ CLI/compiler support.
https://devblogs.microsoft.com/cppblog/porting-a-c-cli-project-to-net-core/

#### Solution: ignore APIs with `std::vector<>`

It is possible to generate MOAB binding in C++/CLI, instead of C# then compiled into dotnet assembly, but that takes time (not sure how to setup vc++ project file). 

Trelis plugin does not use `std::vector<>`, most API in `moab.Core.hpp`, there a overloading version using `Range` instead of `std::vector`. For exampele, if `std::vector< EntityHandle >& entities,` is used,  there is another one called `Range& entities` can be used.

So the solution will be just ignore the `std::vector`,  patch `CheckIgnoredDecl.cs` to ignore 


```cs
// for standard alone function, and also class method, 
// public override bool VisitFunctionDecl(Function function)
// for each param of function parameters
    var ptype = param.Type;
    if (ptype.ToString().Contains("std.vector") || ptype.ToString().Contains("std.list"))
    {
        function.ExplicitlyIgnore();
        Diagnostics.Debug("Function '{0}' was ignored due to STL container param decl type {1}",
            function.Name, ptype);
        return false;
    }
```

typeMap can also be used to ignore. but not sure how to add 
```cs
       private bool IsDeclIgnored(Declaration decl)
        {
            var parameter = decl as Parameter;
            if (parameter != null)
            {
                if (parameter.Type.Desugar().IsPrimitiveType(PrimitiveType.Null))
                    return true;

                if (TypeMaps.FindTypeMap(parameter.Type, out var typeMap))
                    return typeMap.IsIgnored;
            }

            return decl.Ignore;
        }
```

Std.cs  has the content: `[assembly:InternalsVisibleTo("MOAB")]`

std::vector may be supported in C#, but there is lots of ignored , may lead to error. 

Another way to ignore is to return `false`
```cs
// add this code will not compile, Vector class has been added by Stdlib.CLI.cs
// Generators/Types/Stdlib.CLI.cs
    [TypeMap("std::vector", GeneratorKind = GeneratorKind.CLI)]
    public partial class Vector : TypeMap
    {
        public override bool IsIgnored { get { return true; } }
    }
```

`CppSharp.Passes.CheckIgnoredDeclsPass` has been run 3 times, why  per transltionUnit pass

`public abstract class GeneratorOutputPass`  in <Pass.cs>,  `HandleBlock()`  no concrete class, yet

`foreach (var pass in driver.Context.GeneratorOutputPasses.Passes)`

TranslationUnits list is a list of headers, but why so many system headers?
foreach pass, check each unit, do visiting
```
        public virtual bool VisitASTContext(ASTContext context)
        {
            foreach (var unit in context.TranslationUnits)
                VisitTranslationUnit(unit);

            return true;
        }
```  

### Other STL type generated

`global::Std.basic_ostream, Std.pair`

remote 2 fuctions with errror, one is below, just remove 
```
            public static implicit operator global::MOAB.Moab.FileOptions(string option_string)
            {
                return new global::MOAB.Moab.FileOptions(option_string);
            }
```


###  other example project using CppSharp: QtSharp


https://github.com/ddobrev/QtSharp/blob/master/QtSharp/QtSharp.cs

            driver.ParserOptions.MicrosoftMode = false;
            driver.ParserOptions.NoBuiltinIncludes = true;
            driver.ParserOptions.TargetTriple = this.qtInfo.Target;
            driver.ParserOptions.Abi = CppAbi.Itanium;
        
### `std::string` is supported? yes

Std-symbols.cpp  does not include head file `<vector>`
```cs
            [SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "imoab_loadmesh", CallingConvention = __CallingConvention.Cdecl)]
            internal static extern int Loadmesh(int* pid, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(CppSharp.Runtime.UTF8Marshaller))] string filename
```

### skip this error?
    Function 'UnknownInterface' was ignored due to external return decl type global::MOAB.moab.UnknownInterface
    Function 'Interface' was ignored due to external return decl type global::MOAB.moab.Interface



### Windows symbol not exported for DLL file

https://github.com/mono/CppSharp/issues/1585

On every platform, clang is used to parse c++ headers, libraries files, not  per-platform G++/MSVC compilers.  So, on Windows, MingW32 ABI is not supported. 

I just add `__dllspec(export)` to some class header files, it helps, even  CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS is still used for MSVC to build DLL

I have modify the CppSharp,  monkey patch to bypass some skipping, and generated the API I needed. 
Generator/Passes/CheckIgnoredDecls.cs

```csharp
            var ret = function.OriginalReturnType;
            var unitFilenName = function.TranslationUnit.FileName;
            string msg;
            if (HasInvalidType(ret.Type, function, out msg)) // tmp diasable this does not work
            {
                bool skipped = false;
                skipped |= ret.ToString().Contains("moab.ErrorCode");  // working
                skipped |= ret.ToString().Contains("moab.Range.const_iterator");  // working
                skipped |= ret.ToString().Contains("moab.Range");  // working
                if (! skipped)
                {
                    function.ExplicitlyIgnore();
                    Diagnostics.Debug("Function '{0}' was ignored due to {1} return decl type {2}",
                        function.Name, msg, ret);
                    return false;
                }
            }
```

Related CppSharp code
```csharp
// Generator/Passes/CheckIgnoreDecls.cs
private bool HasInvalidType(Type type, Declaration decl, out string msg)
{
    // ... skipped 
    var module = decl.TranslationUnit.Module;
    if (Options.DoAllModulesHaveLibraries() &&
        module != Options.SystemModule && ASTUtils.IsTypeExternal(module, type))
    {
        msg = "external";
        return true;
    }
}
   
// ASTUtils.IsTypeExternal
public static bool IsTypeExternal(Module module, Type type)
{
    var typeModule = type.GetModule();
    return typeModule != null && typeModule.Dependencies.Contains(module);
}
```

