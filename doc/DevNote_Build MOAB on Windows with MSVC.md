## Windows installation

This is mainly a collection of notes, not well written and organized.

### win32 dll export symbol

MOAB_EXPORT macro is not widely applied in source code, which is needed by MOABSharp header file parsing, see PR 
https://bitbucket.org/fathomteam/moab/pull-requests/540

```cmake
  add_definitions(/DMOAB_DLL)
  if (NOT DEFINED CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS)
    # https://cmake.org/cmake/help/v3.4/prop_tgt/WINDOWS_EXPORT_ALL_SYMBOLS.html
    # Export all symbols
    set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS TRUE)
  endif ()
```

why in code `#ifdef WIN32`  not  `#ifdef _WIN32` 

## Windows build dependencies

moab package with  mpich/openmpi on conda-forge is only for osx, linux, not for windows

conda has hdf5 and vtk, but no devel header files.



HDF5 must be installed and make dll files available on search PATH

Note: spaceclaim has HDF5 dll but with long name, hdf5-vc140-1_8_14.dll , should not lead to confliction

VTK header files seems not necessary to write about 



### conda failsure to install blas-devel

blas-devel?

> conda install -c conda-forge blas-devel
> Collecting package metadata (current_repodata.json): ...working... done
> Solving environment: ...working... failed with initial frozen solve. Retrying with flexible solve.
> Solving environment: ...working... failed with repodata from current_repodata.json, will retry with next repodata source.
> Collecting package metadata (repodata.json): ...working... done
> Solving environment: ...working... failed with initial frozen solve. Retrying with flexible solve.
> Solving environment: ...working...



###  build with latest HDF5 installation

official HDF5 v1.12.22 windows installation

binary package from conda has the headers

```shell
cmake ..  -DCMAKE_INSTALL_PREFIX=../../ -DCMAKE_BUILD_TYPE=Release -DENABLE_TESTING=OFF -DENABLE_FORTRAN=OFF -DCMAKE_Fortran_COMPILER="D:\\Software\\mingw64\\bin\\gfortran.exe" -DENABLE_HDF5=ON -DHDF5_ROOT="C:\\Program Files\\HDF_Group\HDF5\\1.12.0" -DHDF5_INCLUDE_DIR="C:\\Program Files\\HDF_Group\HDF5\\1.12.0\\include"   -DENABLE_VTK=ON  -DVTK_VERSION="8.2" -DVTK_INCLUDE_DIR="D:\\MyRepo\\FreeCADbuild\\FreeCADLibs_12.1.4_x64_VC15\\vtk8.2\\include" -DVTK_LIBRARY_DIR="D:\\MyRepo\\FreeCADbuild\\FreeCADLibs_12.1.4_x64_VC15\\lib" -DHDF5_hdf5_LIBRARY_RELEASE="C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5_hl.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libzlib.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libszip.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5_cpp.lib"
```

If no error, build MOAB with

`cmake --build .`



>-- Could NOT find HDF5 (missing: HDF5_DIR)
>-- ---   HDF5 Configuration ::
>--         Directory    : C:/Program Files/HDF_Group/HDF5/1.12.0
>--         IS_PARALLEL  : FALSE
>--         INCLUDES     : C:/Program Files/HDF_Group/HDF5/1.12.0/include
>
>--         LIBRARIES    : C:/Program Files/HDF_Group/HDF5/1.12.0/lib/hdf5_hl_fortran.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/hdf5_fortran.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/hdf5_hl.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/hdf5.lib;D:/Software/anaconda3/Library/lib/z.lib
>
>CMake Warning (dev) at D:/Software/anaconda3/Library/share/cmake-3.19/Modules/FindPackageHandleStandardArgs.cmake:426 (message):
>The package name passed to `find_package_handle_standard_args` (HDF5) does
>not match the name of the calling package (HDF5_MOAB).  This can lead to
>problems in calling code that expects `find_package` result variables
>(e.g., `_FOUND`) to follow a certain pattern.
>Call Stack (most recent call first):
>config/FindHDF5_MOAB.cmake:164 (find_package_handle_standard_args)
>CMakeLists.txt:310 (find_package)
>This warning is for project developers.  Use -Wno-dev to suppress it.

  Manually-specified variables were not used by the project:

    HDF5_hdf5_LIBRARY_RELEASE


#### Solution:  modify repo toplevel CMakeLists.txt

```cmake
  find_package( HDF5_MOAB REQUIRED )
 #add following patch, change the path to fit your hdf5 installation
  set(HDF5_LIBRARIES 
	"C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5_hl.lib"
	"C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5.lib"
	"C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libzlib.lib"
	"C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libszip.lib"
	"C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5_cpp.lib"
	) # monkey patch because find_package( HDF5_MOAB REQUIRED ) does not work properly on Windows
  set (MOAB_HAVE_HDF5 ON)

```


### CMake commands output 


config/FindHDF5_MOAB.cmake:164 (find_package_handle_standard_args)

```
-DHDF5_FOUND=ON -DHDF5_INCLUDE_DIR="D:\\Software\\anaconda3\\Library\\include" -DHDF5_LIBRARY_DIR="D:\\Software\\anaconda3\\Library\\lib"  -DHDF5_LIBRARIES="C:/Program Files/HDF_Group/HDF5/1.12.0/lib/hdf5_hl_fortran.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/hdf5_fortran.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/hdf5_hl.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/hdf5.lib;D:/Software/anaconda3/Library/lib/z.lib" -DBUILD_SHARED_LIBS=ON
```

-DHDF5_hdf5_LIBRARY_RELEASE="C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5_hl.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libzlib.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libszip.lib;C:/Program Files/HDF_Group/HDF5/1.12.0/lib/libhdf5_cpp.lib"


https://bitbucket.org/fathomteam/moab/issues/127/moab-and-mingw64-msys2

To use the official says,  "C:/Program Files/HDF_Group/HDF5/1.x.y/cmake" has a folder "hdf5"

>   3. Set the environment variable HDF5_DIR to the installed location of
>      the config files for HDF5. On Windows:
>            HDF5_DIR=C:/Program Files/HDF_Group/HDF5/1.x.y/cmake
>
>      (Note there are no quote characters used on Windows and all platforms
>      use forward slashes)




## cmake  install command 

Beginning with version 3.15, CMake offers an install switch. From the release notes:

> The "cmake(1)" command gained a new "--install" option. This may be used after building a project to run installation without using the generated build system or the native build tool.

Source: https://cmake.org/cmake/help/v3.15/release/3.15.html#id6

So you can use

```
cmake --install <dir> [--prefix <install-dir>]
```

The optional `--prefix` flag lets you override the `CMAKE_INSTALL_PREFIX`.



#### why cmake install does not work ?

"-DCMAKE_BUILD_TYPE=Release"

`cmake --install .` can not find *.dll in Release folder,

Solution: on windows,  must manually copy dll to a place

### VTK_DIR

even build without VTK, vtk mesh are still can be written ,why?

>CMake Warning:
>Manually-specified variables were not used by the project:
>
>HDF5_hdf5_LIBRARY_RELEASE
>VTK_INCLUDE_DIR
>VTK_LIBRARY_DIR
>VTK_VERSION

conda has no vtk-devel,  vtk package has no header files.
Potentially, FreeCAD libpack can be used.
+ header files: D:\MyRepo\FreeCADbuild\FreeCADLibs_12.1.4_x64_VC15\include\vtk-8.2

+ *.dll files: D:\MyRepo\FreeCADbuild\FreeCADLibs_12.1.4_x64_VC15\bin
must be on PATH at runtime

+ *.lib files: D:\MyRepo\FreeCADbuild\FreeCADLibs_12.1.4_x64_VC15\lib 
to link with symbols in *.lib


###  Linkage error: missing symbol

https://bitbucket.org/fathomteam/moab/issues/134/linkage-error-with-visual-studio-2019-with

https://bitbucket.org/fathomteam/moab/issues/127/moab-and-mingw64-msys2



>file-handle.obj : error LNK2001: unresolved external symbol H5T_NATIVE_ULONG_g [D:\MyRepo\DAGMC_win_bld\moab\build\src\MOAB.vcxproj]
>file.obj : error LNK2001: unresolved external symbol H5T_NATIVE_ULONG_g [D:\MyRepo\DAGMC_win_bld\moab\build\src\MOAB.vcxproj]

```
# this does not help
if(MSVC)
    set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS TRUE)
endif()
```



The reason is tracked downed as "libhdf5_cpp.lib" is not detected by cmake (although specify libraries via command line),  modification of CMakeLists.txt seems solve the problem





### Other issue

>[D:\MyRepo\DAGMC_win_bld\moab\build\src\MOAB.vcxproj]
>LINK : warning LNK4098: defaultlib 'MSVCRT' conflicts with use of other libs; use /NODEFAULTLIB:library [D:\MyRepo\DAGMC_win_bld\moab\build\src\MOAB.vcxproj]
>LINK : warning LNK4217: symbol '?midNodesPerType@CN@moab@@2QAY0BM@$$CBEA (public: static unsigned char const (* moab::CN::midNodesPerType)[28])' defined in 'CN.obj' is imported by 'Tqdcfr.obj' in function '"pub
>lic: static int __cdecl moab::CN::HasMidNodes(enum moab::EntityType,int)" (?HasMidNodes@CN@moab@@SAHW4EntityType@2@H@Z)' [D:\MyRepo\DAGMC_win_bld\moab\build\src\MOAB.vcxproj]
>LINK : warning LNK4217: symbol '?TypeDimensionMap@CN@moab@@2QBU?$pair@W4EntityType@moab@@W412@@std@@B (public: static struct std::pair<enum moab::EntityType,enum moab::EntityType> const * const moab::CN::TypeDi
>mensionMap)' defined in 'CN.obj' is imported by 'WriteGMV.obj' in function '"private: enum moab::ErrorCode __cdecl moab::WriteGMV::local_write_mesh(char const *,unsigned __int64,int,bool,bool)" (?local_write_me
>sh@WriteGMV@moab@@AEAA?AW4ErrorCode@2@PEBD_KH_N2@Z)' [D:\MyRepo\DAGMC_win_bld\moab\build\src\MOAB.vcxproj]
>LINK : warning LNK4286: symbol '?TypeDimensionMap@CN@moab@@2QBU?$pair@W4EntityType@moab@@W412@@std@@B (public: static struct std::pair<enum moab::EntityType,enum moab::EntityType> const * const moab::CN::TypeDi
>mensionMap)' defined in 'CN.obj' is imported by 'ReadHDF5.obj' [D:\MyRepo\DAGMC_win_bld\moab\build\src\MOA