
# Solve error in the generated binding

Fixing described in this document has been automated by removing the problematic generated C# source file using Roylin C# compiler API and add back into MOAB_sharpen.cs file.

Todo:
+ FileOptions has missing ctor, on Linux this class is yet generated
> CppSharp developer is working on this
+ Test the fixing on Linux with the latest CppSharp installed from nuget
> low priority

+ `delete_mesh()` is generated as property, that is not correct, although it works
> low priority
+ a patched CppSharp is used on Windows to ignore `std::vector, std::list` in function signature
  this may be done in `MOABSharpFix`

## Missing constructor on Windows only

### Core::Core() ctr is not generated

it is generated on Linux, so copy those below, and work out EntryPoint for MSVC mangled name for `moab::Core::Core()`

copy from Linux version

```c#

__Internal
{
[SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "??0Core@moab@@QEAA@XZ", CallingConvention = __CallingConvention.Cdecl)]
internal static extern __IntPtr ctor(__IntPtr __instance);

}

           /// <summary>constructor</summary>
            public Core()
                : this((void*) null)
            {
                __Instance = Marshal.AllocHGlobal(sizeof(global::MOAB.Moab.Core.__Internal));
                __ownsNativeInstance = true;
                NativeToManagedMap[__Instance] = this;
                __Internal.ctor(__Instance);
                SetupVTables(GetType().FullName == "MOAB.Moab.Core");
            }
```

#### copy 3 methods from InterfaceInternal class to Core class

not needed on Windows v0.11

#### `class GeomTopoTool,  class FileOptions` also have missed ctor

There is no default ctor, but this one
` GeomTopoTool( Interface* impl, bool find_geoments = false, EntityHandle modelRootSet = 0,   bool p_rootSets_vector = true, bool restore_rootSets = true );`

dump symbol 
`dumpbin %MOAB_Dir%\bin\MOAB.dll /symbols > D:\moab_symbol.txt`

and search by regex expression  `\?\?.GeomTopoTool@moab`

```csharp

[SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "??0GeomTopoTool@moab@@QEAA@PEAVInterface@1@_N_K11@Z", CallingConvention = __CallingConvention.Cdecl)]
 internal static extern void ctor(__IntPtr __instance, __IntPtr impl, bool find_geoments, ulong modelRootSet, bool p_rootSets_vector, bool restore_rootSets);

// no such symbol on Windows
 [SuppressUnmanagedCodeSecurity, DllImport("MOAB", EntryPoint = "_ZN4moab12GeomTopoToolC2ERKS0_",  allingConvention = __CallingConvention.Cdecl)]
 internal static extern void cctor(__IntPtr __instance, __IntPtr _0);

            /// <summary>
            /// <para>Constructor (creates a GTT object)\</para>
            /// <para>Construct a GeomTopoTool object and search for geometric EntitySets if they</para>
            /// <para>exist in the provided moab instance.</para>
            /// </summary>
            /// <param name="impl">MOAB instance the GeomTopoTool will operate on.</param>
            /// <param name="find_geoments">
            /// <para>if specified as True, geometric objects in the provided MOAB instance</para>
            /// <para>will be searched for and added to the GTT.</para>
            /// </param>
            /// <param name="modelRootSet">
            /// <para>the GTT will operate only on geometric EntitySets contained by this EntitySet.</para>
            /// <para>If unprovided, the default value for the modelRootSet is the MOAB instance's</para>
            /// <para>root set, which contains everything in the instance.</para>
            /// </param>
            /// <param name="p_rootSets_vector">
            /// <para>determines the storage datastructure used to relate geometric</para>
            /// <para>EntitySets to their OrientedBoundingBox (OBB) Tree roots. If</para>
            /// <para>set to true (default) a vector will be used to store the root</para>
            /// <para>sets along with an EntityHandle offset for fast lookup of the root</para>
            /// <para>sets. If set to false, then a map will be used to link geometric</para>
            /// <para>EntitySets (keys) to the OBB Tree root sets (values).</para>
            /// </param>
            /// <param name="restore_rootSets">
            /// <para>determines whether or not to restore the internal index that</para>
            /// <para>links geomSets to their corresponding OBB Root.  Only relevant if</para>
            /// <para>find_geoments is true. (default = true)</para>
            /// </param>
            public GeomTopoTool(global::MOAB.Moab.Interface impl, bool find_geoments, ulong modelRootSet, bool p_rootSets_vector, bool restore_rootSets)
            {
                __Instance = Marshal.AllocHGlobal(sizeof(global::MOAB.Moab.GeomTopoTool.__Internal));
                __ownsNativeInstance = true;
                NativeToManagedMap[__Instance] = this;
                var __arg0 = impl is null ? __IntPtr.Zero : impl.__Instance;
                __Internal.ctor(__Instance, __arg0, find_geoments, modelRootSet, p_rootSets_vector, restore_rootSets);
            }
```

There seems no copy constructor on Windows, so this code is only for Linux
```c#
            public GeomTopoTool(global::MOAB.Moab.GeomTopoTool _0)
            {
                __Instance = Marshal.AllocHGlobal(sizeof(global::MOAB.Moab.GeomTopoTool.__Internal));
                __ownsNativeInstance = true;
                NativeToManagedMap[__Instance] = this;
                if (ReferenceEquals(_0, null))
                    throw new global::System.ArgumentNullException("_0", "Cannot be null because it is a C++ reference (&).");
                var __arg0 = _0.__Instance;
                __Internal.cctor(__Instance, __arg0);
            }
```

### Constructor Method is incorrectly treated as operator when no ctor generated on Windows

`FileOptions( const char* option_string );`  and this should not be operator, so it should be removed.

```c#
            public static implicit operator global::MOAB.Moab.FileOptions(string option_string)
            {
                return new global::MOAB.Moab.FileOptions(option_string);
            }
```

> Error	CS1503	Argument 1: cannot convert from 'string' to 'MOAB.Moab.FileOptions.__Internal'	MOABSharp	D:\MyData\StepMultiphysics\DAGMC_Plugin\MOABSharp\MOABSharp\MOABSharp.Windows.cs	3769	Active

Similar case for `GeomTopoTool` class
```
            public static implicit operator global::MOAB.Moab.GeomTopoTool(global::MOAB.Moab.Interface impl)
            {
                return new global::MOAB.Moab.GeomTopoTool(impl);
            }
```

operator in RangeIterator class
```
            public static explicit operator global::MOAB.Moab.RangeInserter(global::MOAB.Moab.Range x)
            {
                return new global::MOAB.Moab.RangeInserter(x);
            }
```

## Linux only problem ( with CppSharp 0.10.5)

###  remove some problematic bindings for "maob/UnknownInterface.h"

On Windows, generated bindigns does not have such class, but Linux may have 

> MOAB.cs(16695,103): error CS0432: Alias 'Moab' not found 

This is not partial class, but a global variable in UnknownInterface.h, delete this generated section
```c++
        public unsafe partial class UnknownInterface
        {
            public partial struct __Internal
            {
            }

            /// <summary>
            /// <para>uuid for an unknown interface</para>
            /// <para>this can be used to either return a default interface</para>
            /// <para>or a NULL interface</para>
            /// </summary>
            public static global::MOAB.Moab.MBuuid IDD_MBUnknown { get; } = (global::MOAB.Moab.MBuuid)moab.MBuuid(4109787230U, 10878, 18272, 187, 6, 185, 237, 39, 233, 74, 236);
        }
```

should be something like below, while can just remove this code section for the moment
```c#
        public unsafe partial class Interface
        {
            public partial struct __Internal
            {
            }

            public static global::MOAB.Moab.MBuuid IDD_MBCore { get; } = (global::MOAB.Moab.MBuuid)moab::MBuuid(144010762, 49920, 16389, 189, 246, 195, 78, 247, 31, 90, 82);
        }
```


### To distinguish Linux and Windows

1. Define a preprocessor variable in your `csproj`

   ```cs
   <PropertyGroup Condition=" '$(OS)' == 'Windows_NT' ">
     <DefineConstants>_WINDOWS</DefineConstants>
   </PropertyGroup>
   ```

2. Use that in your code

   ```cs
   #if _WINDOWS
     // your windows stuff
   #else
     // your *nix stuff
   #endif
   ```



## For Windows and Linux

### Unresolved declaration in CppSharp.Passes.ResolveIncompleteDeclsPass

MOAB_win.cs(12569,115): error CS0234: The type or namespace name 'FileOptions' does not exist in the namespace 'MOAB.Moab' (are you missing an assembly reference?) 

The FileOptions is a class, with API using `std::vector`

Pass 'CppSharp.Passes.ResolveIncompleteDeclsPass'
    Unresolved declaration: FileOptions
    Unresolved declaration: SetIterator
    Unresolved declaration: FileOptions

#### Solution:  `module.Headers.Add("FileOptions.hpp");` 
this class is always fwd declared, CppSharp does not know where is the def header file, so just add this header
still does not help,  `class FileOptions` does not got generated in C#, why?  

Explicitly add this header file into MOAB.Gen.cs, before other headers, 
optionally, replace fwd declation by direct header inclusion  `#include "FileOptions.hpp"`



### "Interface.h" cause some problems

Although interface.h can be skipped in CppSharp via `ctx.IgnoreClassWithName("Interface")`,  its derived class's "Core" methods of this interface will not be wrapped.  **Therefore,  this function can NOT be ignored**


```c++
class Interface // kind of pure abstract, but with impl for template functions
{
  template <class IFace> ErrorCode release_interface(IFace* interface)
    { return release_interface_type( typeid(IFace), interface ); }
```
why `InterfaceInternal class` its content is similar with the `abstract Interface` derived class "Core"

### MOAB.Moab.Core class has some CppSharp generation error
moab/Core.hpp

here  `virtual TagGetData()` should be `override`, why not generate properly?
remove by MOABSharpFix and add back into MOAB_Sharpen.cs with manual fix, remove  if CppSharp has fixed

Note:  TagInfo is renamed into Tag

```csharp
            // here  `virtual TagGetData()` should be `override`, why not generate properly?
            public virtual global::MOAB.Moab.ErrorCode TagGetData(global::MOAB.Moab.Tag tag_handle, ref ulong entity_handles, int num_entities, __IntPtr tag_data)
            {
                var ___TagGetDataDelegate = __VTables.GetMethodDelegate<global::MOAB.Delegates.Func_MOAB_moab_ErrorCode___IntPtr___IntPtr_ulongPtr_int___IntPtr>(0, 81);
                var __arg0 = tag_handle is null ? __IntPtr.Zero : tag_handle.__Instance;
                fixed (ulong* __entity_handles1 = &entity_handles)
                {
                    var __arg1 = __entity_handles1;
                    var __ret = ___TagGetDataDelegate(__Instance, __arg0, __arg1, num_entities, tag_data);
                    return __ret;
                }
            }

            public override global::MOAB.Moab.ErrorCode TagGetData(global::MOAB.Moab.Tag tag_handle, global::MOAB.Moab.Range entity_handles, __IntPtr tag_data)
            {
                var ___TagGetData_1Delegate = __VTables.GetMethodDelegate<global::MOAB.Delegates.Func_MOAB_moab_ErrorCode___IntPtr___IntPtr___IntPtr___IntPtr>(0, 82);
                var __arg0 = tag_handle is null ? __IntPtr.Zero : tag_handle.__Instance;
                if (ReferenceEquals(entity_handles, null))
                    throw new global::System.ArgumentNullException("entity_handles", "Cannot be null because it is a C++ reference (&).");
                var __arg1 = entity_handles.__Instance;
                var __ret = ___TagGetData_1Delegate(__Instance, __arg0, __arg1, tag_data);
                return __ret;
            }
```

> MOAB.cs(14227,56): warning CS0114: 'Core.TagGetData(Tag, ref ulong, int, IntPtr)' hides inherited member 'Interface.TagGetData(Tag, ref ulong, int, IntPtr)'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword. [/mnt/windata/MyData/StepMultiphysics/DAGMC_Plugin/MOABSharp/MOABSharp/MOABSharp.csproj]

#### `Core.HONodeAddedRemoved` is a class declared inside Core class
MOAB.cs(13004,41): warning CS0108: 'Core.HONodeAddedRemoved' hides inherited member 'Interface.HONodeAddedRemoved'. Use the new keyword if hiding was intended. [/mnt/windata/MyData/StepMultiphysics/DAGMC_Plugin/MOABSharp/MOABSharp/MOABSharp.csproj]
MOAB.cs(12922,37): error CS0534: 'Core' does not implement inherited abstract member 'Interface.TagGetData(TagInfo, ref ulong, int, IntPtr)' [/mnt/windata/MyData/StepMultiphysics/DAGMC_Plugin/MOABSharp/MOABSharp/MOABSharp.csproj]

`public new unsafe partial class HONodeAddedRemoved : IDisposable`



#### change 3 properties into methods 

`options.UsePropertyDetectionHeuristics = false;` may solve the problem, but this option oes not work

3 methods are treated as properties in `Core` class,  for the latest generation, property does not cause error.

```c++
// Interface.hpp
  //! Deletes all mesh entities from this MB instance
  virtual ErrorCode delete_mesh()=0;

// 2 other methods
```

The generated c# code is here:

```csharp
/// <summary>return the entity set representing the whole mesh</summary>
public abstract ulong RootSet;
{
    get;
}

/// <summary>Deletes all mesh entities from this MB instance</summary>
public abstract global::MOAB.Moab.ErrorCode DeleteMesh
{
    get;
}

public abstract global::MOAB.Moab.TagInfo GlobalIdTag();

/// <summary>
/// <para>Interface to control memory allocation for sequences</para>
/// <para>Provide a factor that controls the size of the sequence that gets allocated.</para>
/// <para>This is typically useful in the parallel setting when a-priori, the number of ghost entities</para>
/// <para>and the memory required for them within the same sequence as the owned entities are unknown.</para>
/// <para>The default factor is 1.0 but this can be appropriately updated at runtime so that we do not</para>
/// <para>have broken sequences.</para>
/// </summary>
public abstract double SequenceMultiplier
{
    get;
}
```

Solution:  
change these 3 or 4  propoeties into methods, and also change those usage below,  replace twice for each
__target.DeleteMesh  ->  __target.DeleteMesh();
__target.RootSet -> __target.RootSet();
__target.SequenceMultiplier  -> __target.SequenceMultiplier()


### remove some problematic bindings

#### 2 methods in Range class used STL, should be just removed

```c#
            public void Print(global::Std.basic_ostream<sbyte, global::Std.CharTraits<sbyte>> s, string indent_prefix = default)
            {
                if (ReferenceEquals(s, null))
                    throw new global::System.ArgumentNullException("s", "Cannot be null because it is a C++ reference (&).");
                var __arg0 = s.__Instance;
                __Internal.Print(__Instance, __arg0, indent_prefix);
            }
```


### "moab/Types.hpp" typedef and `extern const c-string array` 

` ctx.IgnoreHeadersWithName("Types"); ` leads to  absence of lots of function bindings, so  manually delete the problemetic `class Types`.
If needed some helper function can be manually written and insert into "MOAB_Sharpen.cs" the handcrafted helper function collection.

```c++
// moab/Types.hpp
#ifdef __cplusplus
extern const char* const ErrorCodeStr[];
#endif
extern const char* const* const SenseTypeStr;
```
generated C# binding
```csharp

        public unsafe partial class Types
        {
            public partial struct __Internal
            {
            }

            public static string[] ErrorCodeStr
            {
                get
                {
                    var __ptr = (__IntPtr*)global::MOAB.__Symbols.MOAB_so._ZN4moab12ErrorCodeStrE;
                    return string.__CreateInstance(new __IntPtr(__ptr));
                }
            }

            public static string[] DataTypeStr
            {
                get
                {
                    var __ptr = (__IntPtr*)global::MOAB.__Symbols.MOAB_so._ZN4moab11DataTypeStrE;
                    return string.__CreateInstance(new __IntPtr(__ptr));
                }
            }

            public static sbyte** SenseTypeStr
            {
                get
                {
                    var __ptr = (sbyte***)global::MOAB.__Symbols.MOAB_so._ZN4moab12SenseTypeStrE;
                    return *__ptr;
                }
            }
        }

```


## Error when compiling MOAB-Symbols.cpp
compiling Std-symbols.cpp is fine on Linux, but MOAB-Symbols.cpp has some error. 

`g++ -shared MOAB-symbols_formatted.cpp Std-symbols.cpp  -std=c++11 -I $MOAB_DIR/include -L $MOAB_DIR/lib -lMOAB -liMesh -o libMOAB-symbols.so `
/usr/bin/ld: /tmp/ccct7uIG.o: relocation R_X86_64_PC32 against symbol `_ZTVN4moab16UnknownInterfaceE' can not be used when making a shared object; recompile with -fPIC
/usr/bin/ld: final link failed: bad value
collect2: error: ld returned 1 exit status

### Temporary solution:  It does not affect the usage of MOABSharp.dll
but from ErrorCode to Error message is not implemented, while C# reflection can print enum name straightforwardly. 

https://docs.microsoft.com/en-us/dotnet/api/system.enum.tostring?view=net-5.0#System_Enum_ToString

If those enum string names are important, must be written into output file, then this can be done in C# mannually.

// can be removed at least on Windows, these symbols are referred by `class Types` which has been removed
```c#
// code genrated on Windows
namespace MOAB.__Symbols
{
    internal class MOAB
    {
        public static IntPtr _ErrorCodeStr_moab__3QBQEBDB { get; }
        public static IntPtr _DataTypeStr_moab__3QBQEBDB { get; }
        public static IntPtr _SenseTypeStr_moab__3QEBQEBDEB { get; }
        static MOAB()
        {
            var path = "MOAB";
            var image = CppSharp.SymbolResolver.LoadImage(ref path);
            if (image == IntPtr.Zero) throw new System.DllNotFoundException(path);
            _ErrorCodeStr_moab__3QBQEBDB = CppSharp.SymbolResolver.ResolveSymbol(image, "?ErrorCodeStr@moab@@3QBQEBDB");
            _DataTypeStr_moab__3QBQEBDB = CppSharp.SymbolResolver.ResolveSymbol(image, "?DataTypeStr@moab@@3QBQEBDB");
            _SenseTypeStr_moab__3QEBQEBDEB = CppSharp.SymbolResolver.ResolveSymbol(image, "?SenseTypeStr@moab@@3QEBQEBDEB");
        }
    }
}
```