## Question: how  can I compile and where to put the dll compied from generated cpp files

https://github.com/mono/CppSharp/issues/new


Thank you for bring up such a powerful tool. 

I had tried to follow the SDL example to generate bindings for one lib: MOAB
OS: Linux Ubuntu 20.04,  dotnet core 3.1

I also have difficulty  sort out premake5.lua to run SDL example on Ubuntu. so I use dotnet cli to generate bindings (sucessfully, CppSharp is really a powerful tool) and compiling a dummy binding for a simple header when on cpp file are generated. 

For more realistic project, CppSharp generated some cpp file, but I did not know how to compile into so/dll.  

For example, I had generated 3 files but did not know how to compile:
MOAB.cs, Std.cs    ==> MOABSharp.dll  
Std-symbols.cpp     ==> libStd-symbols.so

`g++ -shared Std-symbols.cpp -I $MOAB_DIR/include -L $MOAB_DIR/lib -lMOAB -liMesh -o libStd-symbols.so`

1. what is the target so/dll name it should be for the generated `Std-symbols.cpp`, so later C# assembly can find it?  
CppSharp has already got a "libStd-symbols.so", in the folder of CppSharp.Runtime.dll

2. how to compile `Std-symbols.cpp` if it is not auto compiled,  CppSharp has some cpp header `CppSharp.h` to include and some so to link with

3.  It seems if I did not compile native cpp code, I will not be able to compile the generated binding cs file?

Is there any tutorial/doc for this kind of situation? I found an issue, mentioned std-symbols.
[https://github.com/mono/CppSharp/issues/1348](https://github.com/mono/CppSharp/issues/1348)

4. to deploy my bindings:  should I just put `CppSharp.dll,  CppSharp.Runtime.dll`,  as well as my generated `MOAB.dll` on assembly loadable path


libCppSharp.CppParser.so  ,  libStd-symbols.so     + libMOAB.so (original lib to bind)   on LD_LIBRARY_PATH?
according to name,  libCppSharp.CppParser.so may only needed to generate binding, not needed to run. 

CLI binding dll seems depend only on  CppSharp.dll and  CppSharp.Runtime.dll

##### Used headers




##### Used settings

All default passes, not user pass added yet.

Target: gcc

Other settings 


