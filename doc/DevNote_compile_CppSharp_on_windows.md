
## CppSharp build on Windows 10 with VS2019


**IMPORTANT NOTE: a clean git clone can avoid lots of problem**

Some the trouble was caused by CppSharp repo was outdated, without pulling latest, when following the online latest github documentation.  Some build instruction has change a lot recently.

Do not share source code tree between Linux and Windows, lua build system may complain with strange message, related with system security.  Just git clone in another place, do a fresh build to download the LLVM for the new OS.


### Generate project/make file

curl is on Windows, but 7zip is not on path, however, it has been installed and working, it seems clever enough to detect.

`DownloadDeps.bat` is working, downloaded the llvm

`GenerateProjects.bat` will not give me the choice menu,  open that bat file and dig out the command line.  so I just run that found command directly `"premake5" --file=premake5.lua vs2019`

not sure why there is a gmake target on Windows, but use vs2019 instead.

### Fix compiling error

CLI.Native.vcxproj

>  "..\CppSharp\src\CppParser\Parser.h(10,10): fatal error C1083: Cannot open include file: 'llvm/Object/Archive.h': No such file or directory"

But this has been added to "Configuration proeperties" of the that project, C/C++  -> "Additional Include Directories"
D:\MyRepo\CppSharp\build\scripts\llvm-80c3ea-windows-vs2019-x64-RelWithDebInfo\include

..\..\scripts\llvm-80c3ea-windows-vs2019-x64-Debug\include;..\..\scripts\llvm-80c3ea-windows-vs2019-x64-Debug\clang\include;..\..\scripts\llvm-80c3ea-windows-vs2019-x64-Debug\clang\lib;..\..\scripts\llvm-80c3ea-windows-vs2019-x64-Debug\build\include;..\..\scripts\llvm-80c3ea-windows-vs2019-x64-Debug\build\clang\include;..\..\..\include;..\..\..\src\CppParser;%(AdditionalIncludeDirectories)

We have  folder "llvm-80c3ea-windows-vs2019-x64-RelWithDebInfo",  so change the solution's Configuration from "Debug" into "Release" will solve this problem


1>LLVMFrontendOpenMP.lib(OMPIRBuilder.obj) : warning LNK4099: PDB 'LLVMFrontendOpenMP.pdb' was not found with 'LLVMFrontendOpenMP.lib(OMPIRBuilder.obj)' or at 'D:\MyRepo\CppSharp\build\vs2019\lib\Release_x64\LLVMFrontendOpenMP.pdb'; linking object as if no debug info


### Link DotNet Framework error

24>C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\Microsoft.Common.CurrentVersion.targets(1177,5): error MSB3644: The reference assemblies for .NETFramework,Version=v4.6.1 were not found. To resolve this, install the Developer Pack (SDK/Targeting Pack) for this framework version or retarget your application. You can download .NET Framework Developer Packs at https://aka.ms/msbuild/developerpacks
16>C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\Microsoft.Common.CurrentVersion.targets(1177,5): error MSB3644: The reference assemblies for .NETFramework,Version=v4.7.2 were not found. To resolve this, install the Developer Pack (SDK/Targeting Pack) for this framework version or retarget your application. You can download .NET Framework Developer Packs at https://aka.ms/msbuild/developerpacks


Without admin previledge to install dotnet SDK, there may be a solution

https://github.com/Microsoft/dotnet/tree/master/releases/reference-assemblies



Starting May, 2019 you can build your project on net20 up to net48 (including ne461) any machine with at least MSBuild or the .NET Core SDK installed without the need of Developer Pack installed.

If .NET Core SDK installed in you machine, Add the nuget package Microsoft.NETFramework.ReferenceAssemblies to your project

```xml
    <ItemGroup>
        <PackageReference Include="Microsoft.NETFramework.ReferenceAssemblies" Version="1.0.0-preview.2">
            <IncludeAssets>runtime; build; native; contentfiles; analyzers</IncludeAssets>
            <PrivateAssets>all</PrivateAssets>
        </PackageReference>
    </ItemGroup>
```

The package include all ReferenceAssemblies starting net20 up to net48

These packages enable building .NET Framework projects on any machine with at least MSBuild or the .NET Core SDK installed plus other scenarios.

For more details: https://github.com/Microsoft/dotnet/tree/master/releases/reference-assemblies



https://stackoverflow.com/questions/54380924/reference-assemblies-for-framework-netframework-version-v4-7-1-were-not-found


I was in the same situation and I found a workaround here : https://github.com/Microsoft/msbuild/issues/2728



  The framework 'Microsoft.NETCore.App', version '3.1.0' was not found.
    - The following frameworks were found:
        5.0.2 at [/usr/share/dotnet/shared/Microsoft.NETCore.App]

You have to `export FrameworkPathOverride=/lib/mono/4.5` and the .NETFramework will be found.
`export FrameworkPathOverride=/etc/mono/4.5` on Ubuntu



### Mono on Ubuntu 20.04  (Oct 2020)

`mono --version` is supported
`dotnet --info`  dotnet 5 is not supported



```
# those commands are discovered in GenerateMake.bat and 
./premake5-linux-64 --file=scripts/LLVM.lua download_llvm
./premake5-linux-64 --file=premake5.lua gmake
make -C gmake config=release_x64 verbose=true

```

### Unit test

1. Change directory to `<CppSharp>\build`

RunTests.sh is not working on Mono, for missing `NUnit.Console-3.9.0`  install the latest nuget 

```sh
cd ../deps/
nuget install NUnit.ConsoleRunner
nuget install NUnit.Console
```

then update the path () in the RunTests.sh

```sh
set -e
BUILD_DIR=$(dirname -- $0)
MONO_PATH=$BUILD_DIR/../deps/NUnit.ConsoleRunner.3.12.0/tools \
cp $BUILD_DIR/../deps/NUnit/nunit.framework.* $BUILD_DIR/gmake/lib/Release_*/
mono $BUILD_DIR/../deps/NUnit.ConsoleRunner.3.12.0/tools/nunit3-console.exe -noresult $BUILD_DIR/gmake/lib/Release_*/*Tests*.dll
# note that, `nunit3-console.exe` is in `NUnit.ConsoleRunner.3.12.0`
```



