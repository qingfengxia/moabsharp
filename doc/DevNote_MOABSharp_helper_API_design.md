## Helper API for MOAB

"MOAB_Sharpen.cs" the handcrafted helper function collection to ease the usage, otherwise some generated API is too complicated to use. 

### MOAB C++ default parameter generation

try generation in CppSharp
https://github.com/mono/CppSharp/blob/main/src/Generator/Options.cs

`options.GenerateDefaultValuesForArguments = true;`  but it seems does not help.

### Typedef is just replaced with the original type

why?  C# does not support type alias exported/used by other files/modules.   Type alias by `using shortname=LongName` is only usable within the cs file.

```
// if set MOAB 64bit build
    typedef uint64_t EntityHandle;
    typedef  int64_t EntityID;
```

`EntityHandle.hpp` contains the definition of `EntityHandle` which could be size_t/ulong dep on build config.


`Tag` is a typedef of `TagInfo pointer`, so just use TagInfo C# reference object type. The generated code has some problem, which has been fixed by adding new API `GetTagHandle()` to fix the handle parameter passing.

### map C++ type erased `void*` data into generics in C#

map c++ void* data setter and getter into C# template function as `Core.SetTagData<T>()` and `Core.GetTagData<T>()`. 

T must be value type, but could be topped up to support array data and string types.

```c#
//just like C++, template parameter type can be deducted
 int numEnt = 1;
rval = myMoabInstance.SetTagData(geom_tag, ref handle, dim);
```

### Constant defined by macro in header file

It is weird that those constants are not generated for C#; CppSharp support this kind of macro constant.
`module.Headers.Add("MBTagConventions.hpp");   // macro constants are not exported?`
Manually add definition into `MOAB_Sharpen.cs` as a `static class Moab.Constants`,  
To use these constants, just `static using Moab.Constants;`

#### Solution: Constants in `<MBTagConventions.hpp>` are added  into `MOAB_sharpen.cs`

https://github.com/mono/CppSharp/issues/1108

`typedef moab::TagInfo *moab::Tag`  just treat Tag as TagInfo (reference type)

```c++
/* MATERIAL_SET_TAG_NAME tag:
 * Represents sets of elements having a common material (corresponds to
 * element blocks in ExodusII)
 * size = sizeof(int)
 * type = int
 * value = integer id for this set (block id from ExodusII)
 * default value = -1
 */
#define MATERIAL_SET_TAG_NAME "MATERIAL_SET"

/* DIRICHLET_SET_TAG_NAME tag:
 * Represents dirichlet-type boundary condition, usually contains only mesh vertices
 * (corresponds to nodesets in ExodusII)
 * size = sizeof(int)
 * type = int
 * value = integer id for this set (nodeset id from ExodusII)
 * default value = -1
 */
#define DIRICHLET_SET_TAG_NAME "DIRICHLET_SET"
```

These constants in `MOAB.Constants static class` are added  into `MOAB_sharpen.cs`



### Array is declared as pointer in MOAB C++ API

In C# `double*` is wrapped as `ref double coordinates`, walkaround is take the reference of the first element

`rval = mbcore.CreateVertices(ref vertex_coords[0], (int)NUMVTX, vertex_handles );`


```c++
  virtual ErrorCode create_vertices(const double *coordinates, 
                                      const int nverts,
                                      Range &entity_handles ) = 0;
```

`public override Moab.ErrorCode CreateVertices(ref double coordinates, int nverts, Moab.Range entity_handles);`


```C#
            /// <summary>Creates a vertex based on coordinates.</summary>
            /// <param name="coordinates">Array that has 3 doubles in it.</param>
            /// <param name="entity_handle">EntityHandle representing the newly created vertex in the database.</param>
            /// <remarks>Example:</remarks>
            public override global::MOAB.Moab.ErrorCode CreateVertex(double[] coordinates, ref ulong entity_handle)
            {
                var ___CreateVertexDelegate = __VTables.GetMethodDelegate<global::MOAB.Delegates.Func_MOAB_moab_ErrorCode___IntPtr_doubleArray_ulongPtr>(0, 53);
                if (coordinates == null || coordinates.Length != 3)
                    throw new ArgumentOutOfRangeException("coordinates", "The dimensions of the provided array don't match the required size.");
                fixed (ulong* __entity_handle1 = &entity_handle)
                {
                    var __arg1 = __entity_handle1;
                    var __ret = ___CreateVertexDelegate(__Instance, coordinates, __arg1);
                    return __ret;
                }
            }

            /// <summary>Create a set of vertices with the specified coordinates</summary>
            /// <param name="coordinates">Array that has 3*n doubles in it.</param>
            /// <param name="nverts">Number of vertices to create</param>
            /// <param name="entity_handles">Range passed back with new vertex handles</param>
            public override global::MOAB.Moab.ErrorCode CreateVertices(ref double coordinates, int nverts, global::MOAB.Moab.Range entity_handles)
            {
                var ___CreateVerticesDelegate = __VTables.GetMethodDelegate<global::MOAB.Delegates.Func_MOAB_moab_ErrorCode___IntPtr_doublePtr_int___IntPtr>(0, 54);
                fixed (double* __coordinates0 = &coordinates)
                {
                    var __arg0 = __coordinates0;
                    if (ReferenceEquals(entity_handles, null))
                        throw new global::System.ArgumentNullException("entity_handles", "Cannot be null because it is a C++ reference (&).");
                    var __arg2 = entity_handles.__Instance;
                    var __ret = ___CreateVerticesDelegate(__Instance, __arg0, nverts, __arg2);
                    return __ret;
                }
            }
```



### default function arguments

c# string can be null, corresponding to `NULL const char*`

`const EntityHandle* output_sets`  -> `ref ulong output_sets`  makes it impossible to set nullptr

```c++
  /** Write or export a file. */
  virtual ErrorCode write_file( const char* file_name,
                                  const char* file_type = 0,
                                  const char* options = 0,
                                  const EntityHandle* output_sets = 0,
                                  int num_output_sets = 0,
                                  const Tag* tag_list = 0,
                                  int num_tags = 0 );
```

```c#
/// <summary>Write or export a file.</summary>
public override global::MOAB.Moab.ErrorCode WriteFile(string file_name, string file_type, string options, ref ulong output_sets, int num_output_sets, global::MOAB.Moab.TagInfo tag_list, int num_tags)
{
    var ___WriteFileDelegate = __VTables.GetMethodDelegate<global::MOAB.Delegates.Func_MOAB_moab_ErrorCode___IntPtr__MarshalAs_UnmanagedType_CustomMarshaler__MarshalTypeRef___typeof_CppSharp_Runtime_UTF8Marshaller____string__MarshalAs_UnmanagedType_CustomMarshaler__MarshalTypeRef___typeof_CppSharp_Runtime_UTF8Marshaller____string__MarshalAs_UnmanagedType_CustomMarshaler__MarshalTypeRef___typeof_CppSharp_Runtime_UTF8Marshaller____string_ulongPtr_int___IntPtr_int>(0, 15);
    fixed (ulong* __output_sets3 = &output_sets)
    {
        var __arg3 = __output_sets3;
        var ____arg5 = tag_list is null ? __IntPtr.Zero : tag_list.__Instance;
        var __arg5 = new __IntPtr(&____arg5);
        var __ret = ___WriteFileDelegate(__Instance, file_name, file_type, options, __arg3, num_output_sets, __arg5, num_tags);
        return __ret;
    }
}
```

workaround is
```c#
    EntityHandle h = 0;  // to mimic nullptr  for  "EntityHandle*" in C++
    rval = mbcore.WriteFile("mbex1.vtk", null, null, ref h, 0, null, 0);
```