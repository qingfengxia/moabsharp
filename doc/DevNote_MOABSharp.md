# Developer notes

## Design decisions
### which generator to use: C++/CLI or C# binding?

`std::vector` type mapping is supported by C++/CLI but not C# binding, the previous is a bit too complicate to setup project build, seems must compiled by visual C++, so not cross-platform.  The trelis_DAGMC_plugin seems do not use MOAB API using `std::vector`, therefore, the target dotnet language C# is slected.

On Linux, the official CppSharp can be used to generate MOAB binding (built from source Nov 2020, version about 0.10.5), but on Windows (Feb 2021, version 0.11.1), CppSharp is compiled from source with some monkey patches to filter out API with `std::vector` and unsuppress some API, see doc

### selection dotnet version to compile the binding

It must  be dotnet framework 4.x, which is used by Ansys, not the dotnet core 3.1/5.0

while CppSharp code generator and parser, can be any dotnet version

### C# wrapping of native library can never achieve cross-platform
The binaries DLL(dotnet assembly) files produced by Visual Studio are binary compatible with Mono, so you only need to get these files to your Linux/Unix server. The exception is assembly use unsafe code such as `DllImport`.



Given the generated binding is different slightly. For a given platform, if MOAB C++ ABI maintained ABI stable, the C# wrapping may does not need to recompiled on a specific OS, `DllImport()` relies on dynamic symbol lookup.  For diff MOAB version, this binding needs to be regenerated if MOAB ABI or API changed .

#### C ABI is much easier to maintain compatible

 given function signature (API) is not change, C#'s P/Invoke will dynamic lookup function by function name, it should work even if function has change location within dll image.

```c#
[SuppressUnmanagedCodeSecurity, DllImport("iMesh", EntryPoint = "imesh_getrootset", CallingConvention = __CallingConvention.Cdecl)]
internal static extern void Getrootset(__IntPtr instance, __IntPtr root_set, int* err);
```

### Shared source code of MOABSharp.Windows.cs and MOABSharp.Linux.cs?

Currently, Linux and Windows bindings are built in different CS project.  

Merge 2 projects may be possible in the near future, the first parameter of `DllImport()` can omit the shard library file suffix.

Currently on linux, `DllImport(“MOAB.so”)` use `libMOAB.so`
According to Microsoft, this lib name should be the full name with suffix, but without path.

https://docs.microsoft.com/en-us/dotnet/standard/native-interop/pinvoke

#### cross-platform `DllImport()` Library name variations
> To facilitate simpler cross platform P/Invoke code, the runtime adds the canonical shared library extension (.dll, .so or .dylib) to native library names. On Linux and macOS, the runtime will also try prepending lib. These library names variations are automatically searched when you use APIs that load unmanaged libraries (e.g., DllImportAttribute).
<https://docs.microsoft.com/en-us/dotnet/standard/native-interop/cross-platform>

NativeLibrary API in dotnet 5 and dotnet core 3.x
https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.nativelibrary?view=net-5.0


## C# Binding generation

### generated files
MOAB.cs       
Std.cs
MOAB-symbols.cpp   
Std-symbols.cpp

"MOAB_Sharpen.cs" the handcrafted helper function collection, to make genrated API easier to user.   
MOABSharp.Linux.csproj: 

The geneated "MOAB.cs" has compiler error, it needs to fix it manually into MOABSharp.Linux.cs 

### Tweak code generation

generate single file, MOAB.cs, where `MOAB` is the module name

```csharp
[SuppressUnmanagedCodeSecurity, DllImport("MOAB.so", EntryPoint = "imoab_resolvesharedentities_", CallingConvention = __CallingConvention.Cdecl)]
 internal static extern int ImoabResolvesharedentities(int* pid, int* num_verts, int* marker);
```

#### CppSharp  when function is regarded as property/getter

`size_t Range::size() const; `  `get_xxx() and set_xxx()` will be treated as a property `Xxx`

`delete_mesh()` is treated as a propety, that is not correct!

solution: 

####  RTTI `type_info` may be not enabled

To enable by `parserOptions.EnableRTTI = options.EnableRTTI;`, or  just ignore these 2 C++ API, in MOAB.Gen.cs
`ErrorCode release_interface_type( const std::type_info& iface_type, void* iface )`
`ErrorCode query_interface_type( const std::type_info& iface_type, void*& iface )`

### [Solve Compiler Error in generated bindings](doc/DevNote_fix_compiling_error.md)

The generated binding code needs some manually fixing before it can compile, hopefully it can be fixed automatically somehow in the future (patch, regex replacement, etc )


### UnitTest

MOAB build must have the HDF5 support, 

https://bitbucket.org/fathomteam/moab/src/master/test/imoab_test.cpp

example

https://www.tangiblesoftwaresolutions.com/product_details/cplusplus_to_csharp_converter_details.html

https://bitbucket.org/fathomteam/moab/src/master/examples/beginner/mbex1.cpp


### nuget package by github workflow CI

must also setup authentication key in github console
```yml
      - name: Upload package
        uses: actions/upload-artifact@v2
        with:
          name: CppSharp.nupkg
          path: |
            artifacts/*.nupkg
```

### Deploy bindings

```csharp
#if NET5_0
        Console.WriteLine("Target framework: .NET 5.0");
#elif NET45
        Console.WriteLine("Target framework: .NET Framework 4.5");
#else
        Console.WriteLine("Target framework: .NET Standard 1.4");
#endif
```


NuGet's default configuration is obtained by loading *%AppData%\NuGet\NuGet.config* (Windows) or *$HOME/.local/share* (Linux/macOS), then loading any *nuget.config* or *.nuget\nuget.config* starting from the root of drive and ending in the current directory.

### remove nuget packages

PM> `Uninstall-Package`

ls /home/qxia/.nuget/packages/cppsharp/0.10.5/lib/
CppSharp.AST.dll  CppSharp.dll  CppSharp.Generator.dll  CppSharp.Parser.CLI.dll  CppSharp.Parser.dll  CppSharp.Runtime.dll


### MakeWatertight DAGMC API will not be wrapped

use DAGMC have CLI tool to make mesh water-tight
```c++
#include "make_watertight/MakeWatertight.hpp"
// make_watertight is part of DAGMC "${DAGMC_DIR}/lib" 
  static moab::Core instance;
  mdbImpl = &instance;
  mw = new MakeWatertight(mdbImpl);

  // ...
  if (make_watertight) {
    rval = mw->make_mesh_watertight(file_set, faceting_tol, false);
    CHK_MB_ERR_RET("Could not make the model watertight.", rval);
  }
```