﻿using System;
using System.Runtime.InteropServices;
using MOAB;
using EntityHandle = System.UInt64;

namespace MOAB.Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello MOAB!");

            var myMoabInstance = new Moab.Core();
            Moab.Tag geom_tag = new Moab.Tag();
            string tag_name = "geom_tag_name";
            var rval = myMoabInstance.GetTagHandle<int>(tag_name, 1, Moab.DataType.MB_TYPE_INTEGER,
                                        out geom_tag, Moab.TagType.MB_TAG_SPARSE | Moab.TagType.MB_TAG_CREAT, -1);

            EntityHandle h = 0;  //why needs this?
            int read_tag_data = 0;
            rval = myMoabInstance.GetTagData<int>(geom_tag, ref h, out read_tag_data);
            Console.WriteLine("get data back as {0}", read_tag_data);

            int tag_data = 5;
            rval = myMoabInstance.SetTagData<int>(geom_tag, ref h, tag_data);

            rval = myMoabInstance.GetTagData<int>(geom_tag, ref h, out read_tag_data);
            Console.WriteLine("get data back as {0}", read_tag_data);

            // exception here, hdf5 dll not find during program cleanup/disposion?
            //  Invalid address specified to RtlValidateHeap
        }
    }
}
