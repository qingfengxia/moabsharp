#  C# binding to MOAB

**C# binding to [MOAB](https://bitbucket.org/fathomteam/moab), the Mesh-Oriented datABase**

by Qingfeng Xia

Copyright: UKAEA 2021

License: the open source license as MOAB, LGPLv3

This is a 100% UKAEA job-related work, but we are seeking to be part of MOAB project.

This project is initiated/funded by the development of DAGMC plugin for SpaceClaim

https://github.com/ukaea/SpaceClaim_API_NeutronicsTools

### Temporary note on code
This code is first host internally at https://git.ccfe.ac.uk/qxia/moabsharp

In order to prepare pull-request to MOAB official, to communicate with CppSharp developer, this code is published at
https://qingfengxia@bitbucket.org/qingfengxia/moabsharp.git
This repo (copyright) on bitbucket could be transfered to MOAB team in the future. 

Interaction with MOAB devevopers at
dllexport for visual C++: https://bitbucket.org/fathomteam/moab/issues?status=new&status=open
feature announcement: https://bitbucket.org/fathomteam/moab/issues/136/feature-c-binding-to-moab-c-api-for-dotnet


## Prerequisite

### dotnet core/.net framework SDK

Tested dotnet core 3.1 on Ubuntu 20.04 and net framework 4.x Windows 10, 64bit, target on "netstandard2", so works with all dotnet runtime.  vscode has C# extension, works with dotnet core 5, and also dotnet core 3.1. 

Although dotnet dll built on Ubuntu can be directly used by Dotnet Framework 4.x projects on Windows, if native shared library has been used like `libMOAB.so`, it must be compile on the target OS. Ansys Workbench is built on top of .net framework, not dotnet core. 

### Compile MOAB following official guide

Compile MOAB by **visual studio C++ compiler** (not MinGW g++ compiler!) and export env var `MOAB_DIR`,   where all c++ headers can  `$MOAB_DIR/include`  and  libMOAB.so can be found `$MOAB_DIR/lib`  or MOAB.dll on windows `$MOAB_DIR/bin`. 

Compiling MOAB on Windows is not well documented as Linux, see some modification of CMakeLists.txt to find HDF5 <doc/DevNote_Build MOAB on Windows with MSVC.md>

### Install CppSharp
On Windows, there is nuget package to install, but MOABSharp needs a patched version, must compiled from source.
So visual studio is needed, visual studio build tool should be fine, yet tested.

On Linux: compile from source code (since 0.11 nuget package is available on Linux), following official documentation, either mono or dotnet core 3.1 is working.

Extra step:
1. Define and export env var `CppSharp_DIR`
Environment variable `$(CppSharp_DIR)` is used in `*.csproj` such as <MOABSharp/MOABSharp.csproj>,  instead of hardcoded path.

2. On Linux, `export LD_LIBRARY_PATH` to find “libCppSharp*.so”, which located in folder of env var `$CppSharp_DIR`.
   on Windows, appending  `%CppSharp_DIR%` to user env var `PATH` to find CppSharp.*.dll.


## Generate MOAB binding

### Run and debug the project <MOAB.Gen/MOAB.Gen.csproj>

**MOAB.Gen** project will generate binding into different folder on different OS. 
<MOABSharp/MOAB.cs>
<MOABSharp.Linux/MOAB.cs>

###  fix the generated C# binding to make it compilable

Currently, `MOAB.cs` will be generated but not compilable !.

Run the automated fix by the command  
```
cd MOABSharpFix
dotnet run
```

This project will remove some problematic binding in MOAB.cs and generate  `MOABSharp.Windows.cs` on Windows, or `MOABSharp.Linux.cs` on Linux.

If the binding source files are still not compilable, then manual fix on the generated binding cs code to generate `MOABSharp.dll`.

see procedure to fix at <doc/DevNote_fix_compiler_error.md>

### build and test generate bindings

run the commands in the batch file [build.sh](build.sh), or use `git-bash.exe build.sh` on Windows.

## Introduction to the generated C# API

### `iMesh and iMOAB` C API has been generated

All C-API are available as static method of the class `MOAB.iMOAB`. Note, only class can hold functions in C#.

prefix like `imoab_` in `imoab_initialize()` can be removed as `Initialize()` of static class iMOAB
the class name `iMOAB` comes from header file name `iMOAB.h`

### CppSharp has renamed some API into dotnet style
`Core::create_vertex()`  become  `CreateVertex()` of `class Core`

### passing ref parameter for scalar pointer as C array

see notes in <doc/DevNote_MOABSharp_helper_API_design.md>

### Tag as typedef of `TagInfo*` needs some patch

see notes in <doc/DevNote_TagHandle.md>

### API helper to ease the usage of the generated C# API 
"MOAB_Sharpen.cs" the handcrafted helper function collection to ease the usage, otherwise some generated API is too complicated to use. 

For example, helper function `Initialize(string[] argv)`  to wrap over the generated alien `MOAB.initialize(int argc, sbyte** argv)` C# API. 

`write_mesh()` in C++ has lots of default parameter, but all parameters must be assigned in the generated API `WriteMesh()`

### Examples

<Examples/MOAB_example.csx>  is the translation of MOAB example <https://bitbucket.org/fathomteam/moab/src/master/examples/beginner/mbex1.cpp>

## Developer notes in doc subfolder

### diff ABI for MOAB version and build
see developer note

## Challenges
### Deployment of Windows MOAB build
  CppSharp.Runtime.dll can be installed from nuget on Windows, but can be copied/included into MOABSharp package.

  SpaceClaim has its own HDF5 version (with long name with version as part of file name), MOAB can link with a diff HDF5 version. 

  VTK may also needs to be useful for internal workflow,  MOAB is not compiled with VTK, but can write out vtk file, why?

  C++ runtime: MOAB.dll imports: ucrtbased.dll, MSVCP140d.dll, VCRuntime140D.dll (Those are Debug versions of DLLs) : those VC runtime dll files should be available, otherwise just install from microsoft
 

  LAPACK: mkl_sequential.dll of intel MKL (math kernel library),  found from anaconda 

### Testing:  
lots of tests are needed to confirm the correctness of data exchange between C# and C++.
Especially, `void*` handle types are widely used in MOAB API

## Todo:
+ default parameter generated by CppSharp 
> done
+ automate the binding generation and fixing
> almost done via MOABSharpFix project, may need further enhancement
+ test deployment on another machine, some indirect dependencies will cause problem.
> 

+ collaboration with CppSharp developer to ignore function signature with STL containers